//
//  MapViewController.swift
//  FarmAr
//
//  Created by Vlad on 29.12.2017.
//  Copyright © 2017 Vlad. All rights reserved.
//

import MapKit
import UIKit
import FirebaseDatabase
import FirebaseAuth
import MBProgressHUD

enum CurrentMapType {
    case geoPins
    case userPins
}

class MapViewController: BaseViewController, MKMapViewDelegate, CLLocationManagerDelegate, UserPinDelegate {
    
    
    @IBOutlet weak var mapView: MKMapView!
    var startLocation: CLLocation?
    
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var chooseDateButton: UIButton!
    @IBOutlet weak var addPinButton: UIButton!
    
    @IBOutlet weak var infoViewXConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var pinButton: UIButton!
    @IBOutlet weak var pinLatitudeTextField: FarmArTextField!
    @IBOutlet weak var pinLongitudeTextField: FarmArTextField!
    @IBOutlet weak var pinAltitudeTextField: FarmArTextField!
    @IBOutlet weak var pinNameTextField: FarmArTextField!
    @IBOutlet weak var pinDescriptionTextView: UITextView!
    
    let locationManager = CLLocationManager()
    var mapType: CurrentMapType = .geoPins
    
    var needUpdate: Bool = true
    
    var geoData:[String : Any] = [:] {
        didSet {
            let dictNSMutable = NSMutableDictionary(dictionary: geoData)
            geoDates = dictNSMutable.allKeys.sorted{($0 as! String).localizedCaseInsensitiveCompare($1 as! String) == ComparisonResult.orderedAscending} as! [String]
            if geoDates.count != 0 {
                selectedDate = geoDates.last!
            }
        }
    }
    var geoDates:[String] = []
    var selectedDate: String = "x" {
        didSet {
            chooseDateButton.setTitle(selectedDate, for: .normal)
        }
    }
    
    var userPins:[[String: Any]] = []
    var currentUserPin: [String: Any]?
    var infoOpened: Bool! = false {
        didSet {
            let width = self.view.frame.size.width
            UIView.animate(withDuration: 0.5) {
                self.infoViewXConstraint.constant = self.infoOpened ? width - 240 : width
                self.view.layoutIfNeeded()
                if self.mapType == .geoPins {
                    self.refreshButton.isHidden = self.infoOpened
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 1
        locationManager.headingFilter = 1
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.delegate = self
        locationManager.startUpdatingHeading()
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        
        self.chooseDateButton.layer.cornerRadius = 8
        self.pinDescriptionTextView.layer.cornerRadius = 5
        self.pinDescriptionTextView.layer.borderWidth = 0.5
        self.pinDescriptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.infoView.layer.cornerRadius = 3
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        mapView.showsCompass = true
        mapView.mapType = .hybridFlyover
        
        infoOpened = false
        mapType = .geoPins
        self.refreshButton.isHidden = false
        self.chooseDateButton.isHidden = false
        self.addPinButton.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if needUpdate {
            self.updatePinsIfNeeded()
        }
    }
    
    @IBAction func menuButtonPressed(_ sender: Any) {
        self.closeKeyboard()
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    
    @IBAction func dateButtonPressed() {
        if geoDates.count != 0 {
            infoOpened = false
            let picker = TCPickerView()
            picker.title = "Select Date"
            let values = geoDates.map { TCPickerView.Value(title: $0) }
            picker.values = values
            picker.delegate = self
            picker.selection = .single
            picker.completion = { (selectedIndexes) in
                self.selectedDate = self.geoDates[selectedIndexes.first!]
                self.removePins()
                self.addPinsBySelectedDate()
            }
            picker.show()
        }
        
    }
    
    @IBAction func mapTypeChanged() {
        if mapType == .geoPins {
            mapType = .userPins
            self.refreshButton.isHidden = true
            self.chooseDateButton.isHidden = true
            self.addPinButton.isHidden = false
            
        } else {
            mapType = .geoPins
            self.refreshButton.isHidden = false
            self.chooseDateButton.isHidden = false
            self.addPinButton.isHidden = true
        }
        infoOpened = false
        needUpdate = true
        refreshPins()
    }
    
    @IBAction func addPinButtonPressed() {
        if let location = locationManager.location {
            let newPin: [String: Any] = ["latitude":location.coordinate.latitude,
                                         "longitude":location.coordinate.longitude,
                                         "altitude":location.altitude,
                                         "title":"Pin #",
                                         "color":"#000000",
                                         "description":""]
            
            let detailViewController = self.storyboard!.instantiateViewController(withIdentifier: "UserPinDetailsViewController") as! UserPinDetailsViewController
            detailViewController.currentPin = newPin
            detailViewController.delegate = self
            detailViewController.isNew = true
            self.navigationController?.pushViewController(detailViewController, animated: true)
        }
    }
    
    @IBAction func refreshButtonPressed() {
        needUpdate = true
        refreshPins()
    }
    
    func refreshPins() {
        self.removePins()
        self.updatePinsIfNeeded()
    }
    
    func removePins() {
        self.mapView.annotations.forEach {
            if !($0 is MKUserLocation) {
                self.mapView.removeAnnotation($0)
            }
        }
    }
    
    func updatePinsIfNeeded() {
        if mapType == .geoPins {
            if !UserManager.shared.isHaveFarm() {
                return
            }
            if needUpdate {
                if AppDelegate.getDelegate().reachability.connection != .none {
                    MBProgressHUD .showAdded(to: self.view, animated: true)
                    ApiManager.shared.getGeoPins(success: { (geoData) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.geoData = geoData
                        DataManager.shared.saveGeoData(self.geoData)
                        self.addPinsBySelectedDate()
                    }, fail: { (error) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.showAlert(message: error.localizedDescription)
                    }) { (errorStr) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.showAlert(message: errorStr)
                    }
                } else {
                    self.geoData = DataManager.shared.getGeoData()
                    self.addPinsBySelectedDate()
                }
            } else {
                self.addPinsBySelectedDate()
            }
        } else {
            if needUpdate {
                if AppDelegate.getDelegate().reachability.connection != .none {
                    MBProgressHUD .showAdded(to: self.view, animated: true)
                    ApiManager.shared.getUserPins(success: { (pins) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.userPins = pins
                        self.needUpdate = false
                        DataManager.shared.saveUserPins(self.userPins)
                        self.addPins()
                    }, fail: { (error) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.showAlert(message: error.localizedDescription)
                    }) { (errorStr) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.showAlert(message: errorStr)
                    }
                } else {
                    self.userPins =  DataManager.shared.getUserPins()
                    self.addPins()
                }
            } else {
                addPins()
            }
        }
    }
    
    func addPins() {
        for pin in userPins {
            let latitude = pin["latitude"] as! Double
            let longitude = pin["longitude"] as! Double
            let pinCoordinate = CLLocationCoordinate2DMake(latitude, longitude)
            let color = (pin["color"] as! NSString).hexStringToUIColor
            let annotation = LocationPin()
            annotation.coordinate = pinCoordinate
            annotation.title = pin["title"] as? String
            annotation.color  = color
            annotation.userPin = pin
            mapView.addAnnotation(annotation)
        }
    }
    
    func addPinsBySelectedDate() {
        if var pinsArray = geoData[selectedDate] as? [[String: Any]] {
            if !UserManager.shared.isPremium() {
                pinsArray = pinsArray.filter {
                    if let type = $0["analytics_type"] as? String {
                        if type == "ndvi" {
                            return true
                        }
                    }
                    return false
                }
            }
            for pinDict in pinsArray {
                if let latitude = pinDict["lat"] as? Double, let longitude = pinDict["lng"] as? Double, let type = pinDict["analytics_type"] as? String {
                    
                    var color = UIColor.black
                    if type == "ndwi" {
                        color = UIColor.blue
                    } else if type == "ndvi" {
                        color = UIColor.green
                    }
                    let  pinCoordinate = CLLocationCoordinate2DMake(latitude, longitude)
                    let title = "\(type): \(pinDict["value"] as! Double)"
                    
                    let annotation = LocationPin()
                    annotation.coordinate = pinCoordinate
                    annotation.title = title
                    annotation.color = color
                    annotation.geoPin = pinDict
                    mapView.addAnnotation(annotation)
                }
            }
        }
        
        
        if let pinsArray: NSArray = geoData[selectedDate] as? NSArray {
            for pin in pinsArray {
                if let pinDict = pin as? [String: Any] {
                    if let latitude = pinDict["Lat"] as? NSString, let longitude = pinDict["Lng"] as? NSString {
                        let pinCoordinate = CLLocationCoordinate2DMake(latitude.doubleValue, longitude.doubleValue)
                        let color = (pinDict["color"] as! NSString).hexStringToUIColor
                        let annotation = LocationPin()
                        annotation.coordinate = pinCoordinate
                        annotation.title = pinDict["type"] as? String
                        annotation.color = color
                        annotation.geoPin = pinDict
                        mapView.addAnnotation(annotation)
                    }
                    
                }
            }
        }
    }
    
    @IBAction func selectPinButtonPressed() {
        if currentUserPin != nil {
            let detailViewController = self.storyboard!.instantiateViewController(withIdentifier: "UserPinDetailsViewController") as! UserPinDetailsViewController
            detailViewController.currentPin = currentUserPin!
            detailViewController.delegate = self
            self.navigationController?.pushViewController(detailViewController, animated: true)
        }
    }
    
    
    //MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
    }
    
    //MARK: - MAP methods
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if startLocation == nil {
            startLocation = userLocation.location
            centerMapIn(lat: startLocation!.coordinate.latitude, lon: startLocation!.coordinate.longitude)
        }
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let identifier = "MyPin"
        if annotation is MKUserLocation {
            return nil
        }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            
            annotationView?.canShowCallout = true
            let pin = annotation as! LocationPin
            annotationView?.image = UIImage(named: "pin_small")!.maskWithColor(color: pin.color!)
            
            annotationView?.canShowCallout = true
            
            if let analyticsStr = pin.analyticsString {
                let label = UILabel(frame: CGRect.init(x: 0, y: 0, width: 200, height: 40))
                label.text = analyticsStr
                label.font = UIFont.systemFont(ofSize: 14)
                label.numberOfLines = 0
                annotationView?.detailCalloutAccessoryView = label
                
                let width = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.lessThanOrEqual, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 200)
                label.addConstraint(width)
                
                let height = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 40)
                label.addConstraint(height)
            }
            
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        infoOpened = false
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        centerMapIn(lat: view.annotation!.coordinate.latitude, lon: view.annotation!.coordinate.longitude)
        if let locationPin = view.annotation as? LocationPin {
            if let geoPin = locationPin.geoPin {
                setInfoView(geoPin, nil)
            } else if let userPin = locationPin.userPin {
                setInfoView(nil, userPin)
            }
        } else {
            setInfoView(nil, nil)
        }
        infoOpened = true
    }
    
    func setInfoView(_ geoPin:[String: Any]?, _ userPin:[String: Any]?) {
        if userPin != nil {
            currentUserPin = userPin
            if let latitude = userPin!["latitude"] as? String {
                pinLatitudeTextField.text = latitude
            }
            
            if let longitude = userPin!["longitude"] as? String {
                pinLongitudeTextField.text = longitude
            }
            
            if let altitude = userPin!["altitude"] as? String {
                pinAltitudeTextField.text = altitude
            }
            
            if let name = userPin!["title"] as? String {
                pinNameTextField.text = name
            }
            
            if let description = userPin!["description"] as? String {
                pinDescriptionTextView.text = description
            }
            
            pinButton.isHidden = false
            let color = (userPin!["color"] as! NSString).hexStringToUIColor
            pinButton.setImage(pinButton.image(for: .normal)?.maskWithColor(color: color), for: .normal)
        } else if geoPin != nil {
            if let gp = geoPin {
                currentUserPin = nil
                pinLatitudeTextField.text = "-"
                if let latitude = gp["lat"] as? Double {
                    pinLatitudeTextField.text = "\(latitude)"
                }
                
                pinLongitudeTextField.text = "-"
                if let longitude = gp["lng"] as? Double {
                    pinLongitudeTextField.text = "\(longitude)"
                }
                
                pinAltitudeTextField.text = "-"
                            
                pinNameTextField.text = ""
                if let type = gp["analytics_type"] as? String {
                    pinNameTextField.text = type
                    var color = UIColor.black
                    if type == "ndwi" {
                        color = UIColor.blue
                    } else if type == "ndvi" {
                        color = UIColor.green
                    }
                    pinButton.setImage(pinButton.image(for: .normal)?.maskWithColor(color: color), for: .normal)
                }
                
                pinDescriptionTextView.text = ""
                if let value = gp["value"] as? Double {
                    pinDescriptionTextView.text = "\(value)"
                }

                pinButton.isHidden = false
            }
            
            
        } else {
            currentUserPin = nil
            pinLatitudeTextField.text = String(mapView.userLocation.location!.coordinate.latitude)
            pinLongitudeTextField.text = String(mapView.userLocation.location!.coordinate.longitude)
            pinAltitudeTextField.text = String(mapView.userLocation.location!.altitude)
            pinNameTextField.text = "User"
            pinDescriptionTextView.text = "You are here."
            pinButton.isHidden = true
        }
    }
    
    func centerMapIn(lat:Double,lon:Double){
        let latitude:CLLocationDegrees = lat
        let longitude:CLLocationDegrees = lon
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        let latDelta:CLLocationDegrees = 0.01
        let lonDelta:CLLocationDegrees = 0.01
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        if mapView.region.span.latitudeDelta <= 0.01 {
            var region = self.mapView.region
            region.center = location
        } else {
            var region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
            region.center = location
            self.mapView.setRegion(region, animated: true)
        }
    }
    
    //MARK: UserPinDelegate
    func pinDeleted(pin: [String: Any]) {
        refreshPins()
    }
    
    func pinEdited(pin: [String: Any]) {
        refreshPins()
    }
}

extension MapViewController: TCPickerViewDelegate {
    func pickerView(_ pickerView: TCPickerView, didSelectRowAtIndex index: Int) {
        
    }
}

