//
//  MainViewController.swift
//  FarmAr
//
//  Created by Vlad on 09.12.2017.
//  Copyright © 2017 Vlad. All rights reserved.
//

import Foundation
import UIKit
import MMDrawerController
 
class MainViewController: MMDrawerController {
    
    var menuViewController: MenuViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.openDrawerGestureModeMask = .all
        self.closeDrawerGestureModeMask = .all
        self.setMaximumLeftDrawerWidth(self.view.frame.width*1/3, animated: true, completion: nil)
        
        let storyboard = UIStoryboard(name: "Main",  bundle: Bundle.main)
        menuViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
        self.leftDrawerViewController = menuViewController

        let centerViewController = UINavigationController.init(rootViewController: storyboard.instantiateViewController(withIdentifier: "PinsViewController") as! PinsViewController)
        self.centerViewController = centerViewController
    }
    
    
}
