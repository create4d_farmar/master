//
//  BaseViewController.swift
//  FarmAr
//
//  Created by Vlad on 09.12.2017.
//  Copyright © 2017 Vlad. All rights reserved.
//

import Foundation
import UIKit
import SwiftValidator
import MBProgressHUD

class BaseViewController: UIViewController {

    var keyboardObservers: [NSObjectProtocol] = []
    var fields: [UIResponder] {
        return []
    }
    
    var activeField: UIView! {
        for field in self.fields {
            if field.isFirstResponder {
                return field as! UIView
            }
        }
        return nil
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func closeKeyboard() {
        for responder in fields {
            responder.resignFirstResponder()
        }
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        for (field, error) in errors {
            if let field = field as? UITextField {
                if field.isKind(of: FarmArTextField.self) {
                    let tsField = field as! FarmArTextField
                    tsField.setErrorMessage(error.errorMessage)
                }
            }
        }
    }
    
    func showAlert(message: String) {
        let alertController = UIAlertController(title: "farmAR", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func randomString(length: Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    func getImageFromView(view: UIView) -> UIImage? {
        view.isHidden = false
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        UIColor.blue.setFill()
        let snapshotImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        view.isHidden = true
        return snapshotImage
    }
}

extension BaseViewController: UITextViewDelegate {
    private func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.isKind(of: FarmArTextField.self) {
            let tsTextField = textField as! FarmArTextField
            tsTextField.validationSuccessful()
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if var index = fields.index(of: textField) {
            index += 1
            while index < fields.count {
                let nextField = fields[index]
                if (nextField as! UIView).isHidden == false {
                    nextField.becomeFirstResponder()
                    break
                }
                index += 1
            }
        }
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    //MARK: Dowloader
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            DispatchQueue.main.async {
                completion(data, response, error)
            }
            }.resume()
    }
}


