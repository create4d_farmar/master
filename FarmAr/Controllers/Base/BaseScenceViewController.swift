//
//  BaseScenceViewController.swift
//  FarmAr
//
//  Created by Vlad on 21.12.2017.
//  Copyright © 2017 Vlad. All rights reserved.
//

import Foundation
import SceneKit
import MapKit
import MBProgressHUD

class BaseScenceViewController: BaseViewController, MKMapViewDelegate, SceneLocationViewDelegate {
    @IBOutlet weak var sceneLocationView: SceneLocationView!
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var pinView:UIView!
    @IBOutlet weak var pinViewImageView: UIImageView!
    @IBOutlet weak var pinViewLabel: UILabel!
    @IBOutlet weak var pinViewPhotoView: UIImageView!
    
    var updateUserLocationTimer: Timer?
    var showMapView: Bool = false
    var centerMapOnUserLocation: Bool = true
    var displayDebugging = false
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var mapButton: UIButton!
    
    var updateInfoLabelTimer: Timer?
    
    var adjustNorthByTappingSidesOfScreen = false
    
    var lastLocation: CLLocation?
    var latDelta: Double! = 0.0
    var lonDelta: Double! = 0.0
    var altDelta: Double! = 0.0
    
    func translateCoord(coord:CLLocationCoordinate2D, metersLat:Double, metersLong:Double) -> CLLocationCoordinate2D {
        
        var tempCoord: CLLocationCoordinate2D = coord
        
        let tempRegion = MKCoordinateRegionMakeWithDistance(coord, metersLat, metersLong)
        let tempSpan = tempRegion.span
        
        tempCoord.latitude = coord.latitude + tempSpan.latitudeDelta
        tempCoord.longitude = coord.longitude + tempSpan.longitudeDelta
        
        return tempCoord;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sceneLocationView.showAxesNode = false
        sceneLocationView.locationDelegate = self
        
        if displayDebugging {
            sceneLocationView.showFeaturePoints = true
        }
        
        sceneLocationView.orientToTrueNorth = true
        sceneLocationView.locationEstimateMethod = .coreLocationDataOnly
        
        mapView.showsUserLocation = true
        mapView.alpha = 0.8
        mapView.isHidden = showMapView
        mapView.mapType = .hybridFlyover
        
        updateInfoLabelTimer = Timer.scheduledTimer(
            timeInterval: 0.1,
            target: self,
            selector: #selector(PinsViewController.updateInfoLabel(updateLocation:)),
            userInfo: nil,
            repeats: true)
        
        updateUserLocationTimer = Timer.scheduledTimer(
            timeInterval: 0.5,
            target: self,
            selector: #selector(PinsViewController.updateUserLocation),
            userInfo: nil,
            repeats: true)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sceneLocationView.run()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneLocationView.pause()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        infoLabel.frame = CGRect(x: 6, y: 0, width: self.view.frame.size.width - 12, height: 12 * 5)
        
        let y = showMapView ? self.view.frame.size.height / 2 : self.view.frame.size.height
        infoLabel.frame.origin.y = y - infoLabel.frame.size.height
        mapButton.frame.origin.y = y - mapButton.frame.size.height - 12
        mapButton.frame.origin.x = self.view.frame.size.width - 75
        mapView.frame = CGRect(
            x: 0,
            y: showMapView ? self.view.frame.size.height / 2 : self.view.frame.size.height,
            width: self.view.frame.size.width,
            height: self.view.frame.size.height / 2)
        
    }

    func removePins() {
        sceneLocationView.removeAllLocationNodes()
        self.mapView.annotations.forEach {
            if !($0 is MKUserLocation) {
                self.mapView.removeAnnotation($0)
            }
        }
    }
    
    @IBAction func mapButtonPressed() {
        showMapView = !showMapView
        UIView.animate(withDuration: 0.5, animations: {
            let y = self.showMapView ? self.view.frame.size.height / 2 : self.view.frame.size.height
            self.infoLabel.frame.origin.y = y - self.infoLabel.frame.size.height
            self.mapButton.frame.origin.y = y - self.mapButton.frame.size.height - 12
            self.mapButton.frame.origin.x = self.view.frame.size.width - 75
            self.mapView.frame = CGRect(
                x: 0,
                y: self.showMapView ? self.view.frame.size.height / 2 : self.view.frame.size.height,
                width: self.view.frame.size.width,
                height: self.view.frame.size.height / 2)
        })
        
    }
    
    @objc func updateUserLocation() {
        
        if let currentLocation = sceneLocationView.currentLocation() {
            DispatchQueue.main.async {
                
                if let bestEstimate = self.sceneLocationView.bestLocationEstimate(),
                    let position = self.sceneLocationView.currentScenePosition() {
                    //                    //                    DDLogDebug("")
                    //                    //                    DDLogDebug("Fetch current location")
                    //                    //                    DDLogDebug("best location estimate, position: \(bestEstimate.position), location: \(bestEstimate.location.coordinate), accuracy: \(bestEstimate.location.horizontalAccuracy), date: \(bestEstimate.location.timestamp)")
                    //                    //                    DDLogDebug("current position: \(position)")
                    //
                    let translation = bestEstimate.translatedLocation(to: position)
                    //
                    //                    //                    DDLogDebug("translation: \(translation)")
                    //                    //                    DDLogDebug("translated location: \(currentLocation)")
                    //                    //                    DDLogDebug("")
                }
                
                if self.centerMapOnUserLocation {
                    UIView.animate(withDuration: 0.45, delay: 0, options: UIViewAnimationOptions.allowUserInteraction, animations: {
                        self.mapView.setCenter(currentLocation.coordinate, animated: false)
                    }, completion: {
                        _ in
                        self.mapView.region.span = MKCoordinateSpan(latitudeDelta: 0.0005, longitudeDelta: 0.0005)
                    })
                }
                
            }
        }
    }
    
    @objc func updateInfoLabel(updateLocation:Bool) {
        if let position = sceneLocationView.currentScenePosition() {
            infoLabel.text = "x: \(String(format: "%.2f", position.x)), y: \(String(format: "%.2f", position.y)), z: \(String(format: "%.2f", position.z))\n"
        }
        
        if let eulerAngles = sceneLocationView.currentEulerAngles() {
            infoLabel.text!.append("Euler x: \(String(format: "%.2f", eulerAngles.x)), y: \(String(format: "%.2f", eulerAngles.y)), z: \(String(format: "%.2f", eulerAngles.z))\n")
        }
        
        if let heading = sceneLocationView.locationManager.heading,
            let accuracy = sceneLocationView.locationManager.headingAccuracy {
            infoLabel.text!.append("Heading: \(String(format:"%.2f", heading))º, accuracy: \(Int(round(accuracy)))º\n")
        }
        
//        if let nodesDistances = sceneLocationView.currentDistances() {
//            let formatter = NumberFormatter()
//            formatter.minimumFractionDigits = 2;
//            formatter.maximumFractionDigits = 2;
//            if nodesDistances.count != 0 {
//                infoLabel.text!.append("\(String(format:"Red:%.2fm Green:%.2fm Blue:%.2fm White:%.2fm\n", nodesDistances[0], nodesDistances[1], nodesDistances[2], nodesDistances[3]))")
//            }
//        }
        
        if let currentLocation = sceneLocationView.locationManager.currentLocation {
            if lastLocation == nil {
                infoLabel.text!.append("\(String(format:"Lat:%.6f Lon:%.6f Alt:%.6f\n", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude, currentLocation.altitude))")
            } else {
                latDelta = lastLocation!.coordinate.latitude - currentLocation.coordinate.latitude
                lonDelta = lastLocation!.coordinate.longitude - currentLocation.coordinate.longitude
                altDelta = lastLocation!.altitude - currentLocation.altitude
                infoLabel.text!.append("\(String(format:"Lat:%@%f Lon:%@%f Alt:%@%f\n", latDelta>=0 ? "+":"", latDelta, lonDelta>=0 ? "+":"", lonDelta, altDelta>=0 ? "+":"", altDelta))")
            }
        }
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
    }
    
    //MARK: MKMapViewDelegate
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let identifier = "MyPin"
        
        if annotation is MKUserLocation {
            return nil
        }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        
            annotationView?.canShowCallout = true
            let pin = annotation as! LocationPin
            annotationView?.image = UIImage(named: "pin_small")!.maskWithColor(color: pin.color!)
            
            annotationView?.canShowCallout = true
            
            if let analyticsStr = pin.analyticsString {
                let label = UILabel(frame: CGRect.init(x: 0, y: 0, width: 200, height: 40))
                label.text = analyticsStr
                label.font = UIFont.systemFont(ofSize: 14)
                label.numberOfLines = 0
                annotationView?.detailCalloutAccessoryView = label
                
                let width = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.lessThanOrEqual, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 200)
                label.addConstraint(width)
                
                let height = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 40)
                label.addConstraint(height)
            }
            
           

        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
    }
    //MARK: SceneLocationViewDelegate
    
    func sceneLocationViewDidAddSceneLocationEstimate(sceneLocationView: SceneLocationView, position: SCNVector3, location: CLLocation) {
        //        DDLogDebug("add scene location estimate, position: \(position), location: \(location.coordinate), accuracy: \(location.horizontalAccuracy), date: \(location.timestamp)")
        //        self.addPins()
    }
    
    func sceneLocationViewDidRemoveSceneLocationEstimate(sceneLocationView: SceneLocationView, position: SCNVector3, location: CLLocation) {
        //        DDLogDebug("remove scene location estimate, position: \(position), location: \(location.coordinate), accuracy: \(location.horizontalAccuracy), date: \(location.timestamp)")
    }
    
    func sceneLocationViewDidConfirmLocationOfNode(sceneLocationView: SceneLocationView, node: LocationNode) {
    }
    
    func sceneLocationViewDidSetupSceneNode(sceneLocationView: SceneLocationView, sceneNode: SCNNode) {
        
    }
    
    func sceneLocationViewDidUpdateLocationAndScaleOfLocationNode(sceneLocationView: SceneLocationView, locationNode: LocationNode) {
        
    }
}

extension DispatchQueue {
    func asyncAfter(timeInterval: TimeInterval, execute: @escaping () -> Void) {
        self.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(timeInterval * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: execute)
    }
}

extension UIView {
    func recursiveSubviews() -> [UIView] {
        var recursiveSubviews = self.subviews
        
        for subview in subviews {
            recursiveSubviews.append(contentsOf: subview.recursiveSubviews())
        }
        
        return recursiveSubviews
    }
}

