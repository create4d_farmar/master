//
//  UserPinDetailsViewController.swift
//  FarmAr
//
//  Created by Vlad on 21.12.2017.
//  Copyright © 2017 Vlad. All rights reserved.
//

import Foundation
import UIKit
import SwiftValidator
import FirebaseAuth
import MBProgressHUD
import FirebaseDatabase
import FirebaseStorage

protocol UserPinDelegate {
    var userPins:[[String: Any]] {get set}
    func pinDeleted(pin: [String: Any])
    func pinEdited(pin: [String: Any])
}

class UserPinDetailsViewController: BaseViewController, KeyboardSubscriber, ValidationDelegate, ImagePicker {    
    
    var delegate: UserPinDelegate?
    var isNew: Bool = false
    
    var currentPin: [String: Any]!
    var currentImage: UIImage?
    var currentColor: UIColor?
    var pinColor: UIColor = UIColor.black
    var infoOpened: Bool! = true {
        didSet {
            var delta: CGFloat = 0.0
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 1136:
                    print("iPhone 5 or 5S or 5C")
                    delta = 40.0
                case 1334:
                    print("iPhone 6/6S/7/8")
                    delta = 40.0
                case 2208:
                    print("iPhone 6+/6S+/7+/8+")
                    delta = 40.0
                case 2436:
                    print("iPhone X")
                    delta = 120.0
                default:
                    print("unknown")
                }
            }
            let width = self.view.frame.width
            UIView.animate(withDuration: 0.5) {
                self.saveButton.alpha = self.infoOpened ? 1 : 0
                self.removeButton.alpha = self.infoOpened ? 1 : 0
                self.infoView.alpha = self.infoOpened ? 1 : 0
                self.infoViewXConstraint.constant = self.infoOpened ? width - 240 - delta : width - delta
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var infoViewXConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var pinButton: UIButton!
    @IBOutlet weak var pinLatitudeTextField: FarmArTextField!
    @IBOutlet weak var pinLongitudeTextField: FarmArTextField!
    @IBOutlet weak var pinAltitudeTextField: FarmArTextField!
    @IBOutlet weak var pinNameTextField: FarmArTextField!
    @IBOutlet weak var pinDescriptionTextView: UITextView!
    
    let validator = Validator()
    let picker: UIImagePickerController = UIImagePickerController()
    let colorPicker = ColorPickerViewController()
    
    override var fields: [UIResponder] {
        return [pinLatitudeTextField, pinLongitudeTextField, pinAltitudeTextField, pinNameTextField, pinDescriptionTextView]
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        validator.registerField(pinLatitudeTextField, rules: [RequiredRule()])
        validator.registerField(pinLongitudeTextField, rules: [RequiredRule()])
        validator.registerField(pinAltitudeTextField, rules: [RequiredRule()])
        validator.registerField(pinNameTextField, rules: [RequiredRule()])
        
        self.backButton.layer.cornerRadius = 5
        self.infoView.layer.cornerRadius = 5
        self.saveButton.layer.cornerRadius = 5
        self.removeButton.layer.cornerRadius = 5
        self.photoButton.layer.cornerRadius = 5
        self.pinDescriptionTextView.layer.cornerRadius = 5
        self.pinDescriptionTextView.layer.borderWidth = 0.5
        self.pinDescriptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.setState()
        infoOpened = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        deregisterForKeyboardNotifications()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.closeKeyboard()
        self.navigationController?.popViewController(animated: true)
    }
    
    func setState() {
        
//        let width = self.view.frame.size.width
//        self.infoViewXConstraint.constant = self.infoOpened ? width - 240 - 40 : width
        
        if let latitude = currentPin["latitude"] as? Double {
            pinLatitudeTextField.text = "\(latitude)"
        }
        
        if let longitude = currentPin["longitude"] as? Double {
            pinLongitudeTextField.text = "\(longitude)"
        }
        
        if let altitude = currentPin["altitude"] as? Double {
            pinAltitudeTextField.text = "\(altitude)"
        }
        
        if let name = currentPin["title"] as? String {
            pinNameTextField.text = name
        }
        
        if let description = currentPin["description"] as? String {
            pinDescriptionTextView.text = description
        }
        
        let color = (currentPin["color"] as! NSString).hexStringToUIColor
        pinButton.setImage(pinButton.image(for: .normal)?.maskWithColor(color: color), for: .normal)
        
        if var imageUrlString = currentPin["original_image_url"] as? String  {
//            imageUrlString = mainURL + imageUrlString
            let url = URL(string: imageUrlString)!
            getDataFromUrl(url: url) { (data, response, error) in
                if error != nil {
                    print(error?.localizedDescription)
                }
                if data != nil {
                    let image = UIImage(data: data!)
                    self.imageView.image = image
                }
            }
        }
    }
    
    @IBAction func selectImageButtonPressed() {
        self.closeKeyboard()
        picker.delegate = self
        self.presentPickerInController(root: self)
    }
    
    func didChangeImage(_ image: UIImage) {
        currentImage = image
        imageView.image = image
    }
    
    @IBAction func infoButtonPressed(_ sender: UIButton) {
        self.closeKeyboard()
        infoOpened = !infoOpened
    }
    
    @IBAction func pinButtonPressed(_ sender: UIButton) {
        self.closeKeyboard()
        colorPicker.autoDismissAfterSelection = true
        colorPicker.scrollDirection = .horizontal
        colorPicker.style = .square
        var colors = [UIColor]()
        let path = Bundle.main.path(forResource: "colorPalette", ofType: "plist")
        let pListArray = NSArray(contentsOfFile: path!)
        
        if let colorPalettePlistFile = pListArray as? [String] {
            for col in colorPalettePlistFile{
                colors.append(UIColor(hex: col))
            }
        }
        colorPicker.allColors = colors
        colorPicker.selectedColor = { color in
            sender.setImage(sender.image(for: .normal)?.maskWithColor(color: color), for: .normal)
            self.currentColor = color
        }
        
        if let popoverController = colorPicker.popoverPresentationController{
            popoverController.delegate = colorPicker
            popoverController.permittedArrowDirections = .any
            popoverController.sourceView = sender
            popoverController.sourceRect = sender.bounds
        }
        
        self.present(colorPicker, animated: true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        validator.validate(self)
    }
    
    func validationSuccessful() {
        self.closeKeyboard()
        pinLatitudeTextField.validationSuccessful()
        pinLongitudeTextField.validationSuccessful()
        pinAltitudeTextField.validationSuccessful()
        pinNameTextField.validationSuccessful()
        
        savePin()
    }
    
    func savePin() {
        currentPin["latitude"] = pinLatitudeTextField.text!
        currentPin["longitude"] = pinLongitudeTextField.text!
        currentPin["altitude"] = pinAltitudeTextField.text!
        currentPin["title"] = pinNameTextField.text!
        currentPin["description"] = pinDescriptionTextView.text!
        
        if currentColor != nil {
            currentPin["color"] = currentColor?.hexColor
        }

        MBProgressHUD.showAdded(to: self.view, animated: true)
        if isNew {
            ApiManager.shared.createUserPin(pin: currentPin!, pinImage: currentImage, success: { (newPin) in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.currentPin = newPin
                self.isNew = false
                self.delegate?.pinEdited(pin: self.currentPin)
                self.showAlert(message: "Pin created successfully.")
            }, fail: { (error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.showAlert(message: error.localizedDescription)
            }) { (errorStr) in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.showAlert(message: errorStr)
            }
        } else {
            let id = currentPin["id"] as! Int
            if let _ = currentPin["user"] as? [String: Any] {
                currentPin.removeValue(forKey: "user")
            }
            ApiManager.shared.editUserPinById(id: id, pin: currentPin, pinImage: currentImage, success: { (newPin) in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.currentPin = newPin
                self.delegate?.pinEdited(pin: self.currentPin)
                self.showAlert(message: "Pin edited successfully.")
            }, fail: { (error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.showAlert(message: error.localizedDescription)
            }) { (errorStr) in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.showAlert(message: errorStr)
            }
        }
    }
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        if isNew {
            self.showAlert(message: "This is new (unsaved) pin.")
            return
        }
        let alert = UIAlertController(title: "farmAR", message: "Are you sure you want to delete this pin?", preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            self.deletePin()
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func deletePin() {
        
        let id = currentPin["id"] as! Int
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ApiManager.shared.deleteUserPinById(id: id, success: {
             MBProgressHUD.hide(for: self.view, animated: true)
            self.delegate?.pinDeleted(pin: self.currentPin)
            self.backButtonPressed(UIButton.init())
        }, fail: { (error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            self.showAlert(message: error.localizedDescription)
        }) { (errorStr) in
            MBProgressHUD.hide(for: self.view, animated: true)
            self.showAlert(message: errorStr)
        }
        
        if let pins = delegate?.userPins {
            if let id = currentPin["id"] as? String {
                let predicate = NSPredicate(format: "%K != %@", "id", id)
                let sortedPins = pins.filter({
                    return predicate.evaluate(with: $0)
                })
                MBProgressHUD.showAdded(to: self.view, animated: true)
                let ref = Database.database().reference()
                let user = Auth.auth().currentUser!
                ref.child("user").child(user.uid).updateChildValues([kUserPinsDBKey: sortedPins], withCompletionBlock: { (error, ref) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let error = error {
                        self.showAlert(message: error.localizedDescription)
                        return
                    }
                    self.delegate?.userPins = sortedPins
                    self.delegate?.pinDeleted(pin: self.currentPin)
                    self.backButtonPressed(UIButton.init())
                })
            }
        }
    }
}

extension UserPinDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        AppDelegate.getDelegate().photoPicking = false
        self.dismiss(animated: true) {
            if let newImage = image.resizeWith(percentage: 0.25) {
                self.didChangeImage(newImage)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        AppDelegate.getDelegate().photoPicking = false
        self.dismiss(animated: true) {
            
        }
    }
}



