//
//  AvatarPicker.swift

import Foundation
import UIKit

protocol ImagePicker: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var imageView: UIImageView! { get }
    var picker: UIImagePickerController { get }
    func selectImageButtonPressed()
    func didChangeImage(_ image: UIImage)
}

extension ImagePicker {
    func presentPickerInController(root: UIViewController) {
        let alertController = UIAlertController(title: "Select Photo Mode", message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                AppDelegate.getDelegate().photoPicking = true
                self.picker.sourceType = .camera
                root.present(self.picker, animated: true, completion: nil)
            } else {
                self.noCameraInController(root)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Library", style: .default, handler: { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                AppDelegate.getDelegate().photoPicking = true
                self.picker.sourceType = .photoLibrary
                root.present(self.picker, animated: true, completion: nil)
            } else {
                self.noLibraryInController(root)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        root.present(alertController, animated: true, completion: nil)
    }
    
    func noCameraInController(_ root: UIViewController) {
        let alertVC = UIAlertController(
            title: "farmAR",
            message: "Sorry, camera not avaliable or camera permission not granted.",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        root.present(alertVC, animated: true, completion: nil)
    }
    
    func noLibraryInController(_ root: UIViewController) {
        let alertVC = UIAlertController(
            title: "farmAR",
            message: "Sorry, photo library permission not granted.",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        root.present(alertVC, animated: true, completion: nil)
    }
}
