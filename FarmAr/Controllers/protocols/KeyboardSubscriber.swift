//
//  KeyboardSubscriber.swift


import UIKit

protocol KeyboardSubscriber: class {
    var keyboardObservers: [NSObjectProtocol] { get set }
    var view: UIView! { get }
    var activeField: UIView! { get }
    var scrollView: UIScrollView! { get}
    func registerForKeyboardNotifications()
    func deregisterForKeyboardNotifications()
    func keyboardWasShown(_ notification: Notification)
    func keyboardWillBeHidden(_ notification: Notification)
}

extension KeyboardSubscriber {
    func registerForKeyboardNotifications() {
        self.keyboardObservers = [
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardDidShow, object: nil, queue: nil) { [unowned self] notification in
                self.keyboardWasShown(notification)
            },
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: nil) { [unowned self] notification in
                self.keyboardWillBeHidden(notification)
            }
        ]
    }
    
    func deregisterForKeyboardNotifications() {
        for observer in keyboardObservers {
            NotificationCenter.default.removeObserver(observer)
        }
    }
    
    func keyboardWasShown(_ notification: Notification) {
        let keyboardSize = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue!.size
        let contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
        self.scrollView.contentInset = contentInset
        self.scrollView.scrollIndicatorInsets = contentInset
        if let activeField = self.activeField {
            let frame = activeField.convert(activeField.bounds, to: self.view)
            let diff = frame.maxY - (view.window!.frame.maxY - keyboardSize.height) + 44
            if diff > 0 {
                self.scrollView.setContentOffset(CGPoint(x: 0, y: self.scrollView.contentOffset.y + diff), animated: true)
            }
        }
    }
    
    func keyboardWillBeHidden(_ notification: Notification) {
        let contentInset = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
        self.scrollView.scrollIndicatorInsets = contentInset
    }
}
