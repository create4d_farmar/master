//
//  UserPinsViewController.swift
//  FarmAr
//
//  Created by Vlad on 20.12.2017.
//  Copyright © 2017 Vlad. All rights reserved.
//

import Foundation
import UIKit
import SceneKit
import MapKit
import FirebaseDatabase
import FirebaseAuth
import MBProgressHUD

class UserPinsViewController: BaseScenceViewController, UserPinDelegate {
    var needUpdate: Bool = true
    
    //    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addPinButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    
    var userPins:[[String: Any]] = []
    var mapSelectedPin: [String: Any]?
    var startLocation: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        needUpdate = true
        if needUpdate {
            self.updatePins()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addPinButton.frame.origin.x = self.view.frame.size.width - 75
    }
    
    @IBAction func menuButtonPressed(_ sender: Any) {
        self.closeKeyboard()
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    
    func refreshPins() {
        needUpdate = true
        self.removePins()
        self.updatePins()
    }
    
    func updatePins() {
        if needUpdate {
            if AppDelegate.getDelegate().reachability.connection != .none {
                MBProgressHUD .showAdded(to: self.view, animated: true)
                ApiManager.shared.getUserPins(success: { (pins) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.userPins = pins
                    DataManager.shared.saveUserPins(self.userPins)
                    self.needUpdate = false
                    self.addPins()
                }, fail: { (error) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showAlert(message: error.localizedDescription)
                }) { (errorStr) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showAlert(message: errorStr)
                }
            } else {
                self.userPins = DataManager.shared.getUserPins()
                self.needUpdate = false
                self.addPins()
            }
        } else {
            addPins()
        }
    }
    
    func addPins() {
        for pin in userPins {
            let latitude = pin["latitude"] as! Double
            let longitude = pin["longitude"] as! Double
            let pinCoordinate = CLLocationCoordinate2DMake(latitude, longitude)
            let altitude = pin["altitude"] as! Double
            let color = (pin["color"] as! NSString).hexStringToUIColor
            let title = pin["title"] as! String
            let pinLocation = CLLocation(coordinate: pinCoordinate, altitude: altitude - 1)

            
            if var imageUrlString = pin["original_image_url"] as? String {
//                imageUrlString = mainURL + imageUrlString
                let url = URL(string: imageUrlString)!
                getDataFromUrl(url: url) { (data, response, error) in
                    if error != nil {
                        print(error?.localizedDescription)
                    }
                    if data != nil {
                        let image = UIImage(data: data!)
                        let pinImage = UIImage(named: "pin")!.maskWithColor(color: color)
                        self.pinViewImageView.image = pinImage
                        self.pinViewLabel.text = title
                        self.pinViewPhotoView.image = image
                        let pinLayer = self.getImageFromView(view: self.pinView)
                        
                        let pinImageNode = LocationAnnotationNode.init(location: pinLocation, pinImage: pinLayer)
                        self.sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: pinImageNode)
                    }
                }
                
            } else {
                let pinImage = UIImage(named: "pin")!.maskWithColor(color: color)
                self.pinViewImageView.image = pinImage
                self.pinViewLabel.text = title
                self.pinViewPhotoView.image = UIImage()
                let pinLayer = self.getImageFromView(view: self.pinView)
                
                let pinImageNode = LocationAnnotationNode.init(location: pinLocation, pinImage: pinLayer)
                self.sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: pinImageNode)
            }
            
            let annotation = LocationPin()
            annotation.coordinate = pinCoordinate
            annotation.title = title
            annotation.color  = color
            annotation.userPin = pin
            mapView.addAnnotation(annotation)
        }
    }
    
    @IBAction func addPinButtonPressed() {
        if let location = sceneLocationView.currentLocation() {
            let newPin: [String: Any] = ["latitude":location.coordinate.latitude,
                       "longitude":location.coordinate.longitude,
                       "altitude":location.altitude,
                       "title":"Pin #",
                       "color":"#000000",
                       "description":""]
            
            let detailViewController = self.storyboard!.instantiateViewController(withIdentifier: "UserPinDetailsViewController") as! UserPinDetailsViewController
            detailViewController.currentPin = newPin
            detailViewController.delegate = self
            detailViewController.isNew = true
            self.navigationController?.pushViewController(detailViewController, animated: true)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        if let touch = touches.first {
            if touch.view != nil {
                if (mapView == touch.view! ||
                    mapView.recursiveSubviews().contains(touch.view!)) {
                    centerMapOnUserLocation = false
                } else {
                    
                    let location = touch.location(in: self.view)
                    
                    if location.x <= 40 && adjustNorthByTappingSidesOfScreen {
                        print("left side of the screen")
                        sceneLocationView.moveSceneHeadingAntiClockwise()
                    } else if location.x >= view.frame.size.width - 40 && adjustNorthByTappingSidesOfScreen {
                        print("right side of the screen")
                        sceneLocationView.moveSceneHeadingClockwise()
                    } else {
                        //                        let image = UIImage(named: "pin")!
                        //                        let annotationNode = LocationAnnotationNode(location: nil, image: image)
                        //                        annotationNode.scaleRelativeToDistance = true
                        //                        sceneLocationView.addLocationNodeForCurrentPosition(locationNode: annotationNode)
                    }
                }
            }
        }
    }
    
    //MARK: Map
    override func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        mapSelectedPin = nil
    }
    
    override func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let locationPin = view.annotation as? LocationPin {
            if let userPin = locationPin.userPin {
                mapSelectedPin = userPin
            }
        }
        let rec = UITapGestureRecognizer.init(target: self, action: #selector(callOutTapped))
        view.addGestureRecognizer(rec)
    }
    
    func callOutTapped() {
        if let userPin = mapSelectedPin {
            let detailViewController = self.storyboard!.instantiateViewController(withIdentifier: "UserPinDetailsViewController") as! UserPinDetailsViewController
            detailViewController.currentPin = userPin
            detailViewController.delegate = self
            self.navigationController?.pushViewController(detailViewController, animated: true)
        }   
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if startLocation == nil {
            startLocation = userLocation.location
            centerMapIn(lat: startLocation!.coordinate.latitude, lon: startLocation!.coordinate.longitude)
        }
    }
    
    func centerMapIn(lat:Double,lon:Double){
        let latitude:CLLocationDegrees = lat
        let longitude:CLLocationDegrees = lon
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        let latDelta:CLLocationDegrees = 0.01
        let lonDelta:CLLocationDegrees = 0.01
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        if mapView.region.span.latitudeDelta <= 0.01 {
            var region = self.mapView.region
            region.center = location
        } else {
            var region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
            region.center = location
            self.mapView.setRegion(region, animated: true)
        }
    }
    
    //MARK: UserPinDelegate
    func pinDeleted(pin: [String: Any]) {
        refreshPins()
    }
    
    func pinEdited(pin: [String: Any]) {
        refreshPins()
    }
}



