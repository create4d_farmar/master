//
//  LoginViewController.swift
//  FarmAr
//
//  Created by Vlad on 09.12.2017.
//  Copyright © 2017 Vlad. All rights reserved.
//

import Foundation
import UIKit
import SwiftValidator
import FirebaseAuth
import MBProgressHUD
import FBSDKLoginKit

private enum Mode: Int {
    case login = 0, signUp = 1, forgotPass = 2
}

class LoginViewController: BaseViewController, KeyboardSubscriber, ValidationDelegate, FBSDKLoginButtonDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var backgroundViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginField: FarmArTextField!
    @IBOutlet weak var passwordField: FarmArTextField!
    @IBOutlet weak var confimrPasswordField: FarmArTextField!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var signUpLabel: UILabel!
//    @IBOutlet var fbButton: FBSDKLoginButton!
    
    let validator = Validator()
    
    fileprivate var currentState: Mode = .login {
        didSet {
            if currentState == .login {
                forgotPasswordButton.setTitle("Forgot Password?", for: .normal)
                forgotPasswordButton.isHidden = true
                signUpLabel.text = "Dont have Account?"
                signUpButton.setTitle("Sign Up", for: .normal)
                loginButton.setTitle("Sign In", for: .normal)
                passwordField.isHidden = false
                confimrPasswordField.isHidden = true
                validator.registerField(self.loginField, rules: [RequiredRule(), EmailRule(message: "Invalid email")])
                validator.registerField(self.passwordField, rules: [RequiredRule(), MinLengthRule(length:6)])
                validator.unregisterField(self.confimrPasswordField)
            } else if currentState == .signUp {
                forgotPasswordButton.isHidden = true
                signUpLabel.text = "Already have Account?"
                signUpButton.setTitle("Sign In", for: .normal)
                loginButton.setTitle("Sign Up", for: .normal)
                passwordField.isHidden = false
                confimrPasswordField.isHidden = false
                forgotPasswordButton.isHidden = true
                validator.registerField(self.loginField, rules: [RequiredRule(), EmailRule(message: "Invalid email")])
                validator.registerField(self.passwordField, rules: [RequiredRule(), MinLengthRule(length:6), ConfirmationRule(confirmField:self.confimrPasswordField)])
                validator.registerField(self.confimrPasswordField, rules: [RequiredRule(), MinLengthRule(length:6), ConfirmationRule(confirmField:self.passwordField)])
            } else if currentState == .forgotPass {
                forgotPasswordButton.isHidden = true
                forgotPasswordButton.setTitle("Back to Sign In", for: .normal)
                passwordField.isHidden = true
                confimrPasswordField.isHidden = true
                loginButton.setTitle("Reset Password", for: .normal)
                validator.registerField(self.loginField, rules: [RequiredRule(), EmailRule(message: "Invalid email")])
                validator.unregisterField(self.passwordField)
                validator.unregisterField(self.confimrPasswordField)
            }
        }
    }
    
    override var fields: [UIResponder] {
        if currentState == .login {
            return [loginField, passwordField]
        } else if currentState == .signUp {
            return [loginField, passwordField, confimrPasswordField]
        } else if currentState == .forgotPass {
            return [loginField]
        }
        return []
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentState = .login
//        fbButton.delegate = self
//        fbButton.readPermissions = ["email"]
        loginButton.layer.cornerRadius = 2
        
        
        #if DEBUG
        loginField.text = "test1000m@gmail.com"
        passwordField.text = "123456"
        
        #endif
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        deregisterForKeyboardNotifications()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        backgroundViewHeightConstraint.constant = view.frame.height - loginButton.frame.height
    }
    
    @IBAction func signInButtonPressed() {
        if  currentState == .login {
            currentState = .signUp
        } else {
            currentState = .login
        }
    }
    
    @IBAction func forgotPassButtonPressed() {
        if  currentState == .login {
            currentState = .forgotPass
        } else {
            currentState = .login
        }
        
    }
    
    @IBAction func privacyButtonPressed() {
        closeKeyboard()
        if let url = URL.init(string: "http://berilsirmacek.com/create4d/privacypolicy.html") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func loginButtonPressed() {
        validator.validate(self)
    }
    
    
    override func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        super.validationFailed(errors)
    }
    
    func validationSuccessful() {
        let delegate = AppDelegate.getDelegate()
        if currentState == .login {
            self.loginField.validationSuccessful()
            self.passwordField.validationSuccessful()
            if let email = self.loginField.text, let password = self.passwordField.text {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                ApiManager.shared.signIn(email: email, password: password, success: { (user) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    UserManager.shared.setUser(user: user)
                    delegate.setState()
                }, fail: { (error) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showAlert(message: error.localizedDescription)
                }) { (errorStr) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showAlert(message: errorStr)
                }
            }
        } else if currentState == .signUp {
            self.loginField.validationSuccessful()
            self.passwordField.validationSuccessful()
            self.confimrPasswordField.validationSuccessful()
            if let email = self.loginField.text, let password = self.passwordField.text {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                ApiManager.shared.signUp(email: email, password: password, success: { (user) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    UserManager.shared.setUser(user: user)
                    delegate.setState()
                }, fail: { (error) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showAlert(message: error.localizedDescription)
                }) { (errorStr) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showAlert(message: errorStr)
                }
            }
        } else if currentState == .forgotPass {
            self.loginField.validationSuccessful()
            if let email = self.loginField.text {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                Auth.auth().sendPasswordReset(withEmail: email, completion: { (error) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if let error = error {
                        self.showAlert(message: error.localizedDescription)
                        return
                    }
                })
            }
        }
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error?) {
        if let error = error {
            showAlert(message: error.localizedDescription)
            return
        }
        
        if result.isCancelled {
            return
        }
        
        let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Auth.auth().signIn(with: credential) { (user, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let error = error {
                self.showAlert(message: error.localizedDescription)
                FBSDKLoginManager().logOut()
                return
            }
            let delegate = AppDelegate.getDelegate()
            delegate.setState()
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        AppDelegate.getDelegate().logOut()
    }
    
}

