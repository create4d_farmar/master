//
//  FilterViewController.swift
//  FarmAr
//
//  Created by Vlad on 07.01.2018.
//  Copyright © 2018 Vlad. All rights reserved.
//

import Foundation
import UIKit
import MMDrawerController
import FirebaseAuth

protocol FilterViewControllerDelegate {
    func filtersChanged(filters:[String])
}

class FilterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var filterTableView: UITableView!
    
    var delegate: FilterViewControllerDelegate?
    
    var selectedTypes: [String] = []
    var types:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filterTableView.tableFooterView = UIView()
        
        let btnBack = UIButton()
        btnBack.setTitle("Close", for: .normal)
        btnBack.frame = CGRect(x: 0.0, y: 0.0, width: 70.0, height: 30.0)
        btnBack.titleLabel?.font = UIFont(name: "Futura-Medium", size: 17)!
        btnBack.addTarget(self, action: #selector(FilterViewController.closeButtonPressed), for: .touchUpInside)
        
        let leftBackButton = UIBarButtonItem()
        leftBackButton.customView = btnBack
        
        self.navigationItem.leftBarButtonItems = [leftBackButton]
        
        let btnDone = UIButton()
        btnDone.setTitle("Done", for: .normal)
        btnDone.frame = CGRect(x: 0.0, y: 0.0, width: 70.0, height: 30.0)
        btnDone.titleLabel?.font = UIFont(name: "Futura-Medium", size: 17)!
        btnDone.addTarget(self, action: #selector(FilterViewController.doneButtonPressed), for: .touchUpInside)
        
        let doneButton = UIBarButtonItem()
        doneButton.customView = btnDone
        
        self.navigationItem.rightBarButtonItem = doneButton
    }
    
    func closeButtonPressed() {
        self.dismiss(animated: true) {
            //
        }
    }
    
    func doneButtonPressed() {
        delegate?.filtersChanged(filters: selectedTypes)
        self.dismiss(animated: true) {
            //
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FilterTableViewCell.identifier, for: indexPath) as! FilterTableViewCell
        
        cell.titleLabel.text = types[indexPath.row]
        cell.setSelect(selectedTypes.index(of: types[indexPath.row]) != nil)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? FilterTableViewCell {
            if let index = selectedTypes.index(of: cell.titleLabel.text!) {
                selectedTypes.remove(at: index)
            } else {
                selectedTypes.append(cell.titleLabel.text!)
            }
        }
        
        filterTableView.reloadData()
    }
}


