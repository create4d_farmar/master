//
//  FilterTableViewCell.swift
//  FarmAr
//
//  Created by Vlad on 07.01.2018.
//  Copyright © 2018 Vlad. All rights reserved.
//

import Foundation
import UIKit

class FilterTableViewCell: UITableViewCell {
    static let identifier = "FilterTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setSelect(_ selected: Bool) {
        self.titleLabel.font = selected ? UIFont.init(name: "Futura-Bold", size: 16) : UIFont.init(name: "Futura-Medium", size: 14)
        self.backgroundColor = selected ? UIColor.white.withAlphaComponent(0.3) : UIColor.clear
        checkImageView.isHidden = !selected
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
}
