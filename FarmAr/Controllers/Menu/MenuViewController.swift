//
//  MenuViewController.swift
//  FarmAr
//
//  Created by Vlad on 10.12.2017.
//  Copyright © 2017 Vlad. All rights reserved.
//

import Foundation
import UIKit
import MMDrawerController
import FirebaseAuth
import MBProgressHUD

enum MenuRow: Int {
    case MenuRowGeoPins = 0
    case MenuRowMyPins = 1
    case MenuRow2DMap = 2
    case MenuRowFarm = 3
}

class MenuViewController: UIViewController {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var menuTableView: UITableView!
    
    fileprivate var selectedIndex: Int! = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.menuTableView.tableFooterView = UIView()
        self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.height / 2
        self.avatarImageView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(avatarPressed)))
        self.avatarImageView.isUserInteractionEnabled = true
        self.setState()
        NotificationCenter.default.addObserver(self, selector: #selector(setState), name: NSNotification.Name.init(rawValue: kUserUpdateNotif), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setState() {
        if let user = UserManager.shared.user {
            let name = user["name"] ?? "User"
            self.nameLabel.text = "Hello \(name)!"
            if var url = user["original_avatar_url"] as? String {
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: URL(string: url)!)
                    DispatchQueue.main.async {
                        if data != nil {
                            self.avatarImageView.image = UIImage(data: data!)
                        }
                    }
                }
            }
            self.menuTableView.reloadData()
        }
    }
    
    func close() {
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    
    func avatarPressed() {
        self.mm_drawerController.closeDrawer(animated: true, completion: nil)
        MBProgressHUD.showAdded(to: self.mm_drawerController.centerViewController.view, animated: true)
        ApiManager.shared.getUser(success: { (user) in
            MBProgressHUD.hide(for: self.mm_drawerController.centerViewController.view, animated: true)
            UserManager.shared.setUser(user: user)
            self.selectedIndex = -1
            let userViewController = self.storyboard!.instantiateViewController(withIdentifier: "UserViewController") as! UserViewController
            (self.mm_drawerController.centerViewController as! UINavigationController).viewControllers = [userViewController]
        }, fail: { (error) in
            MBProgressHUD.hide(for: self.mm_drawerController.centerViewController.view, animated: true)
        }) { (errorStr) in
            MBProgressHUD.hide(for: self.mm_drawerController.centerViewController.view, animated: true)
        }
    }
    
    @IBAction func didTapSignOutButton(_ sender: UIButton) {
        self.mm_drawerController.closeDrawer(animated: true) { (success) in
            AppDelegate.getDelegate().logOut()
        }
    }
}
extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MenuTableViewCell.identifier, for: indexPath) as! MenuTableViewCell
        if indexPath.row == MenuRow.MenuRowGeoPins.rawValue {
            cell.titleLabel.text = "GEO Tags"
        } else if indexPath.row == MenuRow.MenuRowMyPins.rawValue {
            cell.titleLabel.text = "My Tags"
        } else if indexPath.row == MenuRow.MenuRow2DMap.rawValue {
            cell.titleLabel.text = "2D Map"
        } else if indexPath.row == MenuRow.MenuRowFarm.rawValue {
            if UserManager.shared.isHaveFarm() {
                if let farm = UserManager.shared.userFarm() {
                    cell.titleLabel.text = "Open: \(farm["name"] ?? "Farm")"
                } else {
                    cell.titleLabel.text = "Open: Farm"
                }
            } else {
                cell.titleLabel.text = "Create Farm"
            }
        }
        cell.setSelect(indexPath.row == selectedIndex ? true : false)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        if indexPath.row == MenuRow.MenuRowGeoPins.rawValue {
            let pinsViewController = self.storyboard!.instantiateViewController(withIdentifier: "PinsViewController") as! PinsViewController
            (self.mm_drawerController.centerViewController as! UINavigationController).viewControllers = [pinsViewController]
            self.mm_drawerController.closeDrawer(animated: true, completion: nil)
        } else if indexPath.row == MenuRow.MenuRowMyPins.rawValue {
            let pinsViewController = self.storyboard!.instantiateViewController(withIdentifier: "UserPinsViewController") as! UserPinsViewController
            (self.mm_drawerController.centerViewController as! UINavigationController).viewControllers = [pinsViewController]
            self.mm_drawerController.closeDrawer(animated: true, completion: nil)
        } else if indexPath.row == MenuRow.MenuRow2DMap.rawValue {
            let mapViewController = self.storyboard!.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            (self.mm_drawerController.centerViewController as! UINavigationController).viewControllers = [mapViewController]
            self.mm_drawerController.closeDrawer(animated: true, completion: nil)
        } else if indexPath.row == MenuRow.MenuRowFarm.rawValue {
            if UserManager.shared.isHaveFarm() {
                MBProgressHUD.showAdded(to: self.mm_drawerController.centerViewController.view, animated: true)
                ApiManager.shared.getFarm(success: { (farm) in
                    MBProgressHUD.hide(for: self.mm_drawerController.centerViewController.view, animated: true)
                    UserManager.shared.setUserFarm(farm: farm)
                    let farmVC = self.storyboard!.instantiateViewController(withIdentifier: "FarmBoardsViewController") as! FarmBoardsViewController
                    farmVC.currentFarm = farm
                    (self.mm_drawerController.centerViewController as! UINavigationController).viewControllers = [farmVC]
                    self.mm_drawerController.closeDrawer(animated: true, completion: nil)
                }, fail: { (error) in
                    MBProgressHUD.hide(for: self.mm_drawerController.centerViewController.view, animated: true)
                }) { (errorStr) in
                    MBProgressHUD.hide(for: self.mm_drawerController.centerViewController.view, animated: true)
                }
            } else {
                let newFarm: [String: Any] = ["name":"My First Farm"]
                let farmVC = self.storyboard!.instantiateViewController(withIdentifier: "FarmBoardsViewController") as! FarmBoardsViewController
                farmVC.isNew = true
                farmVC.currentFarm = newFarm
                (self.mm_drawerController.centerViewController as! UINavigationController).viewControllers = [farmVC]
                self.mm_drawerController.closeDrawer(animated: true, completion: nil)
            }
        }
        
        self.menuTableView.reloadData()
    }
}
    

