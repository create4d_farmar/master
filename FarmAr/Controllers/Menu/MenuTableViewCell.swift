//
//  MenuTableViewCell.swift
//  FarmAr
//
//  Created by Vlad on 10.12.2017.
//  Copyright © 2017 Vlad. All rights reserved.
//

import Foundation
import UIKit

class MenuTableViewCell: UITableViewCell {
    static let identifier = "MenuTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel?.textColor = UIColor.white
    }
    
    func setSelect(_ selected: Bool) {
        self.titleLabel.font = selected ? UIFont.init(name: "Futura-Bold", size: 16) : UIFont.init(name: "Futura-Medium", size: 14)
    }
    
    @IBOutlet weak var titleLabel: UILabel!
}
