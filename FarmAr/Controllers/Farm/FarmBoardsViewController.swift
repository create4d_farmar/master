//
//  FarmBoardsViewController.swift
//  farmAR
//
//  Created by Vlad on 11.06.2018.
//  Copyright © 2018 Vlad. All rights reserved.
//

import UIKit
import GoogleMaps
import MBProgressHUD
import SwiftValidator

class FarmBoardsViewController: BaseViewController, GMSMapViewDelegate, CLLocationManagerDelegate, KeyboardSubscriber, ValidationDelegate {
    
    var startLocation: CLLocation?
    
    let locationManager = CLLocationManager()
    var markersArray: [GMSMarker] = []
    var selectedMarker: GMSMarker?
    var measurementPolyline: GMSPolyline?
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var markerView: UIView!
    @IBOutlet weak var markerLatitudeLabel: UILabel!
    @IBOutlet weak var markerLongitudeLabel: UILabel!
    
    @IBOutlet weak var measurementLabel: UILabel!
    
    @IBOutlet weak var radioButton_ndwi: UIButton!
    @IBOutlet weak var radioButton_ndvi: UIButton!
    @IBOutlet weak var radioButton_none: UIButton!
    let tag_ndvi:Int = 1
    let tag_ndwi:Int = 2
    let tag_none:Int = 3
    var image_ndvi = UIImage()
    var image_ndwi = UIImage()
    var currentOverlay: GMSGroundOverlay?
    
    var isNew: Bool = false
    var isMyFarm: Bool = false
    
    var currentFarm: [String: Any]!
    
    var infoOpened: Bool! = true {
        didSet {
            var delta: CGFloat = 0.0
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 1136:
                    print("iPhone 5 or 5S or 5C")
                    delta = 40.0
                case 1334:
                    print("iPhone 6/6S/7/8")
                    delta = 40.0
                case 2208:
                    print("iPhone 6+/6S+/7+/8+")
                    delta = 40.0
                case 2436:
                    print("iPhone X")
                    delta = 120.0
                default:
                    print("unknown")
                }
            }
            let width = self.view.frame.width
            UIView.animate(withDuration: 0.5) {
                self.saveButton.alpha = self.infoOpened ? 1 : 0
                self.removeButton.alpha = self.infoOpened ? 1 : 0
                self.infoView.alpha = self.infoOpened ? 1 : 0
                self.infoViewXConstraint.constant = self.infoOpened ? width - 240 - delta : width - delta
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var infoViewXConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var pinButton: UIButton!
    @IBOutlet weak var farmNameTextField: FarmArTextField!
    @IBOutlet weak var farmDescriptionTextView: UITextView!
    
    let validator = Validator()
    let picker: UIImagePickerController = UIImagePickerController()
    
    override var fields: [UIResponder] {
        return [farmNameTextField]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        markerView.layer.cornerRadius = 5
        markerView.alpha = 0.0
        
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        mapView.mapType = .satellite
        mapView.settings.compassButton = true
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        
        validator.registerField(farmNameTextField, rules: [RequiredRule()])

        self.infoView.layer.cornerRadius = 5
        self.saveButton.layer.cornerRadius = 5
        self.removeButton.layer.cornerRadius = 5
        self.removeButton.isHidden = true
        
        self.setState()
        infoOpened = true
        
        radioButton_ndwi.tag = tag_ndwi
        radioButton_ndvi.tag = tag_ndvi
        radioButton_none.tag = tag_none
        
        radioButton_ndwi.setImage(UIImage(named: "marker_selected")?.maskWithColor(color: UIColor.red), for: .selected)
        radioButton_ndwi.setImage(UIImage(named: "marker_unselected")?.maskWithColor(color: UIColor.black), for: .normal)
        radioButton_ndvi.setImage(UIImage(named: "marker_selected")?.maskWithColor(color: UIColor.red), for: .selected)
        radioButton_ndvi.setImage(UIImage(named: "marker_unselected")?.maskWithColor(color: UIColor.black), for: .normal)
        radioButton_none.setImage(UIImage(named: "marker_selected")?.maskWithColor(color: UIColor.red), for: .selected)
        radioButton_none.setImage(UIImage(named: "marker_unselected")?.maskWithColor(color: UIColor.black), for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        deregisterForKeyboardNotifications()
    }
    
    //MARK: Actions
    @IBAction func menuButtonPressed(_ sender: Any) {
        self.closeKeyboard()
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    
    func setState() {
        isMyFarm = false
        if let owner = self.currentFarm["owner"] as? [String: Any] {
            if let user = owner["user"] as? [String: Any] {
                if let ownerId = user["id"] as? Int, let userId = UserManager.shared.user!["id"] as? Int {
                    if ownerId == userId {
                        isMyFarm = true
                    }
                }
            }
        }
        
        if let name = currentFarm["name"] as? String {
            farmNameTextField.text = name
        }
        
        pinButton.isEnabled = false
        farmNameTextField.isEnabled = false
        infoButton.isEnabled = false
        saveButton.isEnabled = false
        removeButton.isEnabled = false
        
        if isNew {
            pinButton.isEnabled = true
            farmNameTextField.isEnabled = true
            infoButton.isEnabled = true
            saveButton.isEnabled = true
        }
        
        if isMyFarm {
            pinButton.isEnabled = true
            farmNameTextField.isEnabled = true
            infoButton.isEnabled = true
            saveButton.isEnabled = true
            removeButton.isEnabled = true
        }
        
        for marker in markersArray {
            marker.map = nil
        }
        markersArray = []
        if let polygon = currentFarm["polygon"] as? [[String: Any]] {
            for coordinate in polygon {
                if let latitude = coordinate["latitude"] as? Double, let longitude = coordinate["longitude"] as? Double {
                    createMarkerAtPosition(CLLocationCoordinate2D.init(latitude: latitude, longitude: longitude))
                }
            }
            hideMarkerView()
            updateMeasurementView()
            updateMeasurementPolyline()
        }
        
        updateOverlayWithState(state: .none)
        radioButton_ndwi.isEnabled = false
        radioButton_ndvi.isEnabled = false
        
        if UserManager.shared.isPremium() {
            if let ndwi_urlStr = currentFarm["image_ndwi"] as? String {
                if let ndwi_url = URL.init(string: ndwi_urlStr) {
                    getDataFromUrl(url: ndwi_url) { (data, response, error) in
                        if data != nil {
                            let image = UIImage(data: data!)
                            self.image_ndwi = image!
                            self.radioButton_ndwi.isEnabled = true
                        }
                    }
                }
            }
        }
        if let ndvi_urlStr = currentFarm["image_ndvi"] as? String {
            if let ndvi_url = URL.init(string: ndvi_urlStr) {
                getDataFromUrl(url: ndvi_url) { (data, response, error) in
                    if data != nil {
                        let image = UIImage(data: data!)
                        self.image_ndvi = image!
                        self.radioButton_ndvi.isEnabled = true
                    }
                }
            }
        }
    }
    
    @IBAction func infoButtonPressed(_ sender: UIButton) {
        self.closeKeyboard()
        infoOpened = !infoOpened
    }
    
    @IBAction func pinButtonPressed(_ sender: UIButton) {
        self.closeKeyboard()
        
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        
        if markersArray.count < 3 {
            self.showAlert(message: "Mark the boundaries of your farm on the map (at least 3 points).")
            return
        }
        let path = GMSMutablePath()
        for marker in markersArray {
            path.add(marker.position)
        }
        let areaSquare = Double(round(1000*GMSGeometryArea(path))/1000)
        if areaSquare < 1000 {
            self.showAlert(message: "Farm should be more than 1000 square meters")
            return
        }
        
        validator.validate(self)
    }
    
    func validationSuccessful() {
        self.closeKeyboard()
        farmNameTextField.validationSuccessful()
        
        saveFarm()
    }
    
    func saveFarm() {
        let polygon = markersArray.map { (marker) -> [String: Any] in
            return ["latitude": marker.position.latitude, "longitude": marker.position.longitude]
        }
        if isNew {
            if let name = farmNameTextField.text, let lat = startLocation?.coordinate.latitude, let long = startLocation?.coordinate.longitude {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                ApiManager.shared.createFarm(polygon: polygon, name: name, latitude: lat, longitude: long, success: { (farm) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.currentFarm = farm
                    UserManager.shared.setUserFarm(farm: farm)
                    self.isNew = false
                    self.setState()
                    self.showAlert(message: "Farm created successfully.")
                }, fail: { (error) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showAlert(message: error.localizedDescription)
                }) { (errorStr) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showAlert(message: errorStr)
                }
            }
        } else {
            if let name = farmNameTextField.text, let lat = startLocation?.coordinate.latitude, let long = startLocation?.coordinate.longitude {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                ApiManager.shared.editFarm(polygon: polygon, latitude: lat, longitude: long, name: name, success: { (farm) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.currentFarm = farm
                    UserManager.shared.setUserFarm(farm: farm)
                    self.setState()
                    self.showAlert(message: "Farm updated successfully.")
                }, fail: { (error) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showAlert(message: error.localizedDescription)
                }) { (errorStr) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showAlert(message: errorStr)
                }
            }
        }
    }
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        if isNew {
            self.showAlert(message: "This is new (unsaved) farm.")
            return
        }
        let alert = UIAlertController(title: "farmAR", message: "Are you sure you want to delete this farm?", preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            self.deleteFarm()
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }


    func deleteFarm() {
    
    }
    
    enum OverlayState {
        case ndvi
        case ndwi
        case none
    }
    
    @IBAction func radioButtonPressed(_ sender: UIButton) {
        if sender.tag == tag_ndvi {
            updateOverlayWithState(state: .ndvi)
        } else if sender.tag == tag_ndwi {
            updateOverlayWithState(state: .ndwi)
        } else {
            updateOverlayWithState(state: .none)
        }
    }
    
    func updateOverlayWithState(state: OverlayState) {
        if let overlay = currentOverlay {
            overlay.map = nil
            currentOverlay = nil
        }
        switch state {
        case .ndvi:
            radioButton_ndvi.isSelected = true
            radioButton_ndwi.isSelected = false
            radioButton_none.isSelected = false
            currentOverlay = GMSGroundOverlay(bounds: getOverlayBounds(), icon: image_ndvi)
            break
        case .ndwi:
            radioButton_ndvi.isSelected = false
            radioButton_ndwi.isSelected = true
            radioButton_none.isSelected = false
            currentOverlay = GMSGroundOverlay(bounds: getOverlayBounds(), icon: image_ndwi)
            
            break
        case .none:
            radioButton_ndvi.isSelected = false
            radioButton_ndwi.isSelected = false
            radioButton_none.isSelected = true
            break
        }
//        currentOverlay?.bearing = 1
        currentOverlay?.map = mapView
        
    }
    
    func getOverlayBounds() -> GMSCoordinateBounds? {
        if markersArray.count < 3 {
            return nil
        }
        let path = GMSMutablePath()
        for marker in markersArray {
            path.add(marker.position)
        }
        if markersArray.count != 0 {
            path.add(markersArray.first!.position)
        }
        return GMSCoordinateBounds.init(path: path)
    }
    
    //MARK: MarkerView
    func showMarkerView() {
        UIView.animate(withDuration: 0.3) {
            self.markerView.alpha = 1.0
        }
    }
    
    func hideMarkerView() {
        UIView.animate(withDuration: 0.3) {
            self.markerView.alpha = 0.0
        }
    }
    
    func updateMarkerView() {
        if let marker = selectedMarker {
            markerLatitudeLabel.text = "\(marker.position.latitude)"
            markerLongitudeLabel.text = "\(marker.position.longitude)"
        }
    }
    
    @IBAction func deleteMarkerPressed(_ sender: UIButton) {
        if let m = selectedMarker {
            if let index = markersArray.index(of: m) {
                let marker = markersArray[index]
                marker.map = nil
                markersArray.remove(at: index)
                hideMarkerView()
                updateMeasurementView()
                updateMeasurementPolyline()
            }
        }
    }
    
    //MARK: MeasurementView
    func updateMeasurementView() {
        if markersArray.count >= 3 {
            let path = GMSMutablePath()
            for marker in markersArray {
                path.add(marker.position)
            }
            measurementLabel.text = "\(Double(round(1000*GMSGeometryArea(path))/1000))"
        } else {
            measurementLabel.text = "-"
        }
    }
    
    func updateMeasurementPolyline() {
        measurementPolyline?.map = nil
        let path = GMSMutablePath()
        for marker in markersArray {
            path.add(marker.position)
        }
        if markersArray.count != 0 {
            path.add(markersArray.first!.position)
        }
        measurementPolyline = GMSPolyline(path: path)
        measurementPolyline?.strokeWidth = 2.0
        measurementPolyline?.strokeColor = UIColor.green
        
        measurementPolyline?.map = mapView
        
    }
    
    //MARK: GMSMapViewDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if startLocation == nil {
            startLocation = locations.first
            centerMapIn(lat: startLocation!.coordinate.latitude, lon: startLocation!.coordinate.longitude)
        }
    }
    
    func centerMapIn(lat:Double,lon:Double){
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 17.0)
        mapView.camera = camera
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.closeKeyboard()
        if (selectedMarker != nil) {
            selectedMarker?.icon = UIImage.init(named: "marker_unselected")?.maskWithColor(color: UIColor.green)
            selectedMarker = nil
            hideMarkerView()
            return
        }
        
        if !isNew && !isMyFarm {
            return
        }
        createMarkerAtPosition(coordinate)
    }
    
    func createMarkerAtPosition(_ position: CLLocationCoordinate2D) {
        let marker = GMSMarker(position: position)
        marker.icon = UIImage.init(named: "marker_unselected")?.maskWithColor(color: UIColor.green)
        marker.map = mapView
        marker.groundAnchor = CGPoint.init(x: 0.5, y: 0.5)
        marker.isDraggable = isMyFarm || isNew
        markersArray.append(marker)
        updateMeasurementView()
        updateMeasurementPolyline()
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        self.closeKeyboard()
        
        if let m = selectedMarker {
            m.icon = UIImage.init(named: "marker_unselected")
            selectedMarker = nil
        }
        
        marker.icon = UIImage.init(named: "marker_selected")?.maskWithColor(color: UIColor.red)
        selectedMarker = marker
        updateMarkerView()
        showMarkerView()
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        self.closeKeyboard()
        updateMeasurementView()
        updateMeasurementPolyline()
    }
}
