//
//  UserViewController.swift
//  farmAR
//
//  Created by Vlad on 20.05.2018.
//  Copyright © 2018 Vlad. All rights reserved.
//

import UIKit
import SwiftValidator
import MBProgressHUD
import CoreLocation

enum ValidatorMode {
    case userSaving
    case farmCreating
}

class UserViewController: BaseViewController, KeyboardSubscriber, ValidationDelegate, ImagePicker, CLLocationManagerDelegate {
    var imageView: UIImageView!
    var location: CLLocation!
    
    @IBOutlet weak var backgroundViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var userNameTextField: FarmArTextField!
    @IBOutlet weak var farmButton: UIButton!
    
    var currentMode: ValidatorMode = .userSaving
    var userImage: UIImage?
    
    let validator = Validator()
    let picker: UIImagePickerController = UIImagePickerController()
    let locationManager: CLLocationManager = CLLocationManager()
    
    override var fields: [UIResponder] {
        return [userNameTextField]
    }
    
    @IBAction func menuButtonPressed(_ sender: Any) {
        self.closeKeyboard()
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        validator.registerField(userNameTextField, rules: [RequiredRule()])
        self.view.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(closeKeyboard)))
        self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.height / 2
        self.avatarImageView.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(selectImageButtonPressed)))
        
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setState()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        deregisterForKeyboardNotifications()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        backgroundViewHeightConstraint.constant = view.frame.height - farmButton.frame.height
    }

    func setState() {
        if let user = UserManager.shared.user {
            setFarmButtonState()
            if let name = user["name"] as? String {
                userNameTextField.text = name
            }
            if var url = user["avatar"] as? String {
//                url = mainURL + url
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: URL(string: url)!)
                    DispatchQueue.main.async {
                        if data != nil {
                            self.avatarImageView.image = UIImage(data: data!)
                        }
                    }
                }
            }
        }
    }
    
    //MARK: Save User / Validation
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        closeKeyboard()
        currentMode = .userSaving
        validator.validate(self)
    }
    
    func validationSuccessful() {
        userNameTextField.validationSuccessful()
        switch currentMode {
        case .userSaving:
            saveUser()
            break
        case .farmCreating:
            createFarm()
            break
        }
    }
    
    private func saveUser() {
        if let name = userNameTextField.text {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            ApiManager.shared.editUser(userName: name, userImage: userImage, success: { (user) in
                MBProgressHUD.hide(for: self.view, animated: true)
                UserManager.shared.setUser(user: user)
                self.setState()
            }, fail: { (error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.showAlert(message: error.localizedDescription)
            }) { (errorStr) in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.showAlert(message: errorStr)
            }
        }
    }
    
    //MARK: Farm Button
    private func setFarmButtonState() {
        if UserManager.shared.isHaveFarm() {
            if let farm = UserManager.shared.userFarm() {
                farmButton.setTitle("Open: \(farm["name"] ?? "Farm")", for: .normal)
            } else {
                farmButton.setTitle("Open: Farm", for: .normal)
            }
        } else {
            farmButton.setTitle("Create Farm", for: .normal)
        }
    }
    
    @IBAction func farmButtonPressed(_ sender: UIButton) {
        closeKeyboard()
        if UserManager.shared.isHaveFarm() {
            openFarm()
        } else {
            currentMode = .farmCreating
            validator.validate(self)
        }
        
    }
    
    private func createFarm() {
        if location != nil {
            let newFarm: [String: Any] = ["name":"Farm #"]
            
            let farmViewController = self.storyboard!.instantiateViewController(withIdentifier: "FarmBoardsViewController") as! FarmBoardsViewController
            farmViewController.isNew = true
            farmViewController.currentFarm = newFarm
            self.navigationController?.pushViewController(farmViewController, animated: true)
        }
    }
    
    private func openFarm() {
        if let farm = UserManager.shared.userFarm() {
            let farmVC = self.storyboard!.instantiateViewController(withIdentifier: "FarmBoardsViewController") as! FarmBoardsViewController
            farmVC.currentFarm = farm
            self.navigationController?.pushViewController(farmVC, animated: true)
        } else {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            ApiManager.shared.getFarm(success: { (farm) in
                MBProgressHUD.hide(for: self.view, animated: true)
                UserManager.shared.setUserFarm(farm: farm)
                let farmVC = self.storyboard!.instantiateViewController(withIdentifier: "FarmBoardsViewController") as! FarmBoardsViewController
                farmVC.currentFarm = farm
                self.navigationController?.pushViewController(farmVC, animated: true)
            }, fail: { (error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.showAlert(message: error.localizedDescription)
            }) { (errorStr) in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.showAlert(message: errorStr)
            }
        }
    }
    
    //MARK: Avatar Picking
    func selectImageButtonPressed() {
        self.closeKeyboard()
        picker.delegate = self
        picker.allowsEditing = true
        self.presentPickerInController(root: self)
    }
    
    func didChangeImage(_ image: UIImage) {
        userImage = image
        avatarImageView.image = image
    }
    
    //MARK: CLLocationManager
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        
        location = locations.first
    }
}

extension UserViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        AppDelegate.getDelegate().photoPicking = false
        self.dismiss(animated: true) {
            if let newImage = image.resizeWith(percentage: 0.25) {
                self.didChangeImage(newImage)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        AppDelegate.getDelegate().photoPicking = false
        self.dismiss(animated: true) {
            
        }
    }
}
