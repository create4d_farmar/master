//
//  FarmViewController.swift
//  farmAR
//
//  Created by Vlad on 21.05.2018.
//  Copyright © 2018 Vlad. All rights reserved.
//

import UIKit
import CoreLocation
import MBProgressHUD
import SwiftValidator

class FarmViewController: BaseViewController, KeyboardSubscriber, ValidationDelegate, ImagePicker {
    
    var isNew: Bool = false
    var currentFarm: [String: Any]!
    
    var infoOpened: Bool! = true {
        didSet {
            var delta: CGFloat = 0.0
            if UIDevice().userInterfaceIdiom == .phone {
                switch UIScreen.main.nativeBounds.height {
                case 1136:
                    print("iPhone 5 or 5S or 5C")
                    delta = 40.0
                case 1334:
                    print("iPhone 6/6S/7/8")
                    delta = 40.0
                case 2208:
                    print("iPhone 6+/6S+/7+/8+")
                    delta = 40.0
                case 2436:
                    print("iPhone X")
                    delta = 120.0
                default:
                    print("unknown")
                }
            }
            let width = self.view.frame.width
            UIView.animate(withDuration: 0.5) {
                self.saveButton.alpha = self.infoOpened ? 1 : 0
                self.removeButton.alpha = self.infoOpened ? 1 : 0
                self.infoView.alpha = self.infoOpened ? 1 : 0
                self.infoViewXConstraint.constant = self.infoOpened ? width - 240 - delta : width - delta
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var infoViewXConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var pinButton: UIButton!
    @IBOutlet weak var farmLatitudeTextField: FarmArTextField!
    @IBOutlet weak var farmLongitudeTextField: FarmArTextField!
    @IBOutlet weak var farmAltitudeTextField: FarmArTextField!
    @IBOutlet weak var farmNameTextField: FarmArTextField!
    @IBOutlet weak var farmDescriptionTextView: UITextView!
    
    let validator = Validator()
    let picker: UIImagePickerController = UIImagePickerController()
    
    override var fields: [UIResponder] {
        return [farmLatitudeTextField, farmLongitudeTextField, farmAltitudeTextField, farmNameTextField, farmDescriptionTextView]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        validator.registerField(farmLatitudeTextField, rules: [RequiredRule()])
        validator.registerField(farmLongitudeTextField, rules: [RequiredRule()])
        validator.registerField(farmNameTextField, rules: [RequiredRule()])
        
        self.backButton.layer.cornerRadius = 5
        self.infoView.layer.cornerRadius = 5
        self.saveButton.layer.cornerRadius = 5
        self.removeButton.layer.cornerRadius = 5
        
        self.farmDescriptionTextView.layer.cornerRadius = 5
        self.farmDescriptionTextView.layer.borderWidth = 0.5
        self.farmDescriptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.setState()
        infoOpened = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        deregisterForKeyboardNotifications()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.closeKeyboard()
        self.navigationController?.popViewController(animated: true)
    }
    
    func setState() {
        
        if let latitude = currentFarm["latitude"] as? Double {
            farmLatitudeTextField.text = "\(latitude)"
        }
        
        if let longitude = currentFarm["longitude"] as? Double {
            farmLongitudeTextField.text = "\(longitude)"
        }
        
        if let altitude = currentFarm["altitude"] as? Double {
            farmAltitudeTextField.text = "\(altitude)"
        }
        
        if let name = currentFarm["name"] as? String {
            farmNameTextField.text = name
        }
        
        if let description = currentFarm["description"] as? String {
            farmDescriptionTextView.text = description
        }
        
        if var imageUrlString = currentFarm["original_image_url"] as? String  {
//            imageUrlString = mainURL + imageUrlString
            let url = URL(string: imageUrlString)!
            getDataFromUrl(url: url) { (data, response, error) in
                if data != nil {
                    let image = UIImage(data: data!)
                    self.imageView.image = image
                }
            }
        }
        if !isNew {
            if let owner = currentFarm["owner"] as? [String: Any] {
                if let ownerId = owner["id"] as? Int, let userId = UserManager.shared.user!["id"] as? Int {
                    if ownerId != userId {
                        pinButton.isEnabled = false
                        farmLatitudeTextField.isEnabled = false
                        farmLongitudeTextField.isEnabled = false
                        farmNameTextField.isEnabled = false
                        infoButton.isEnabled = false
                        saveButton.isEnabled = false
                    }
                }
            }
        }
        
    }
    
    @IBAction func selectImageButtonPressed() {
        self.closeKeyboard()
        picker.delegate = self
        self.presentPickerInController(root: self)
    }
    
    func didChangeImage(_ image: UIImage) {
//        currentImage = image
        imageView.image = image
    }
    
    @IBAction func infoButtonPressed(_ sender: UIButton) {
        self.closeKeyboard()
        infoOpened = !infoOpened
    }
    
    @IBAction func pinButtonPressed(_ sender: UIButton) {
        self.closeKeyboard()
        
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        validator.validate(self)
    }
    
    func validationSuccessful() {
        self.closeKeyboard()
        farmLatitudeTextField.validationSuccessful()
        farmLongitudeTextField.validationSuccessful()
        farmAltitudeTextField.validationSuccessful()
        farmNameTextField.validationSuccessful()
        
        saveFarm()
    }
    
    func saveFarm() {
        
        if isNew {
            if let lat = Double(farmLatitudeTextField.text!), let long = Double(farmLongitudeTextField.text!), let name = farmNameTextField.text {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                ApiManager.shared.createFarm(latitude: lat, longitude: long, name: name, success: { (farm) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.currentFarm = farm
                    UserManager.shared.setUserFarm(farm: farm)
                    self.isNew = false
                    self.setState()
                    self.showAlert(message: "Farm created successfully.")
                }, fail: { (error) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showAlert(message: error.localizedDescription)
                }) { (errorStr) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showAlert(message: errorStr)
                }
            }
            
        } else {
            if let lat = Double(farmLatitudeTextField.text!), let long = Double(farmLongitudeTextField.text!), let name = farmNameTextField.text {
//                MBProgressHUD.showAdded(to: self.view, animated: true)
//                ApiManager.shared.editFarm(latitude: lat, longitude: long, name: name, success: { (farm) in
//                    MBProgressHUD.hide(for: self.view, animated: true)
//                    self.currentFarm = farm
//                    UserManager.shared.setUserFarm(farm: farm)
//                    self.setState()
//                    self.showAlert(message: "Farm updated successfully.")
//                }, fail: { (error) in
//                    MBProgressHUD.hide(for: self.view, animated: true)
//                    self.showAlert(message: error.localizedDescription)
//                }) { (errorStr) in
//                    MBProgressHUD.hide(for: self.view, animated: true)
//                    self.showAlert(message: errorStr)
//                }
            }
        }
    }
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        if isNew {
            self.showAlert(message: "This is new (unsaved) farm.")
            return
        }
        let alert = UIAlertController(title: "farmAR", message: "Are you sure you want to delete this farm?", preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            
        }
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            self.deleteFarm()
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteFarm() {
        
       
    }
}

extension FarmViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        AppDelegate.getDelegate().photoPicking = false
        self.dismiss(animated: true) {
            if let newImage = image.resizeWith(percentage: 0.25) {
                self.didChangeImage(newImage)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        AppDelegate.getDelegate().photoPicking = false
        self.dismiss(animated: true) {
            
        }
    }
}

