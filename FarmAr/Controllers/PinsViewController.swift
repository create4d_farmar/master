//
//  PinsViewController.swift
//  FarmAr
//
//  Created by Vlad on 10.12.2017.
//  Copyright © 2017 Vlad. All rights reserved.
//

import Foundation
import UIKit
import SceneKit
import MapKit
import FirebaseDatabase
import FirebaseAuth
import MBProgressHUD

class PinsViewController: BaseScenceViewController {
    var needUpdate: Bool = true
    
//    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var chooseDateButton: UIButton!
    
    
    var startLocation: CLLocation?
    var types = ["ph", "leaf density", "weed", "water"]
    var selectedTypes: [String] = []
    
    var geoData:[String : Any] = [:] {
        didSet {
            let dictNSMutable = NSMutableDictionary(dictionary: geoData)
            geoDates = dictNSMutable.allKeys.sorted{($0 as! String).localizedCaseInsensitiveCompare($1 as! String) == ComparisonResult.orderedAscending} as! [String]
            if geoDates.count != 0 {
                selectedDate = geoDates.last!
            }
        }
    }
    var geoDates:[String] = []
    var geoTypes:[String] = []
    var selectedDate: String = "x" {
        didSet {
            chooseDateButton.setTitle(selectedDate, for: .normal)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.chooseDateButton.layer.cornerRadius = 8
        selectedTypes = types
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.updatePins()
        
//        setTempState()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        refreshButton.frame.origin.x = self.view.frame.size.width - 75
    }
    
    @IBAction func menuButtonPressed(_ sender: Any) {
        self.closeKeyboard()
        self.mm_drawerController.toggle(.left, animated: true, completion: nil)
    }
    
    @IBAction func dateButtonPressed() {
        if geoDates.count != 0 {
            let picker = TCPickerView()
            picker.title = "Select Date"
            let values = geoDates.map { TCPickerView.Value(title: $0) }
            picker.values = values
            picker.delegate = self
            picker.selection = .single
            picker.completion = { (selectedIndexes) in
                if selectedIndexes.count != 0 {
                    self.selectedDate = self.geoDates[selectedIndexes.first!]
                    self.removePins()
                    self.addPinsBySelectedDate()
                }
            }
            picker.show()
        }
        
    }
    
    @IBAction func filterButtonPressed() {
        if let filters = storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as? FilterViewController {
            filters.delegate = self
            filters.types = types
            filters.selectedTypes = selectedTypes
            self.present(UINavigationController.init(rootViewController: filters), animated: true, completion: nil)
        }
    }
    
    @IBAction func refreshPins() {
        needUpdate = true
        self.removePins()
        self.updatePins()
    }
    
    @objc func updatePins() {
        if !UserManager.shared.isHaveFarm() {
            return
        }
        
        if needUpdate {
            if AppDelegate.getDelegate().reachability.connection != .none {
                MBProgressHUD .showAdded(to: self.view, animated: true)
                ApiManager.shared.getGeoPins(success: { (geoData) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.geoData = geoData
                    DataManager.shared.saveGeoData(self.geoData)
                    self.addPinsBySelectedDate()
                }, fail: { (error) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showAlert(message: error.localizedDescription)
                }) { (errorStr) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showAlert(message: errorStr)
                }
            } else {
                self.geoData = DataManager.shared.getGeoData()
                self.addPinsBySelectedDate()
            }
        } else {
             self.addPinsBySelectedDate()
        }
    }
    
    func addPinsBySelectedDate() {
        if var pinsArray = geoData[selectedDate] as? [[String: Any]] {
            if !UserManager.shared.isPremium() {
                pinsArray = pinsArray.filter {
                    if let type = $0["analytics_type"] as? String {
                        if type == "ndvi" {
                            return true
                        }
                    }
                    return false
                }
            }
            for pinDict in pinsArray  { 
                if ((sceneLocationView.locationManager.currentLocation?.coordinate) == nil) {
                    needUpdate = true
                    return
                }
                var pinCoordinate: CLLocationCoordinate2D
                var altitude: CLLocationDistance
                var color: UIColor
                
                lastLocation = sceneLocationView.locationManager.currentLocation!
                latDelta = 0.0
                lonDelta = 0.0
                altDelta = 0.0
                if let latitude = pinDict["lat"] as? Double, let longitude = pinDict["lng"] as? Double, let type = pinDict["analytics_type"] as? String {
                    
                    
                    
                    color = UIColor.black
                    if type == "ndwi" {
                        color = UIColor.blue
                    } else if type == "ndvi" {
                        color = UIColor.green
                    }
                    pinCoordinate = CLLocationCoordinate2DMake(latitude, longitude)
                    altitude = (sceneLocationView.locationManager.currentLocation?.altitude)!
                    
                    let pinLocation = CLLocation(coordinate: pinCoordinate, altitude: altitude - 1)
                    print(pinLocation)
                    let title = "\(type): \(pinDict["value"] as! Double)"
                    
                    let pinImage = UIImage(named: "pin")!.maskWithColor(color: color)
                    self.pinViewImageView.image = pinImage
                    self.pinViewLabel.text = title
                    self.pinViewPhotoView.image = UIImage()
                    let pinLayer = self.getImageFromView(view: self.pinView)
                    
                    let pinImageNode = LocationAnnotationNode.init(location: pinLocation, pinImage: pinLayer)
                    self.sceneLocationView.addLocationNodeWithConfirmedLocation(locationNode: pinImageNode)
                    
                    
                    let annotation = LocationPin()
                    annotation.coordinate = pinCoordinate
                    annotation.title = title
                    annotation.color = color
                    mapView.addAnnotation(annotation)
                }
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        if let touch = touches.first {
            if touch.view != nil {
                if (mapView == touch.view! ||
                    mapView.recursiveSubviews().contains(touch.view!)) {
                    centerMapOnUserLocation = false
                } else {
                    
                    let location = touch.location(in: self.view)
                    
                    if location.x <= 40 && adjustNorthByTappingSidesOfScreen {
                        print("left side of the screen")
                        sceneLocationView.moveSceneHeadingAntiClockwise()
                    } else if location.x >= view.frame.size.width - 40 && adjustNorthByTappingSidesOfScreen {
                        print("right side of the screen")
                        sceneLocationView.moveSceneHeadingClockwise()
                    } else {

                    }
                }
            }
        }
    }
    
    
    //MARK: Map
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if startLocation == nil {
            startLocation = userLocation.location
            centerMapIn(lat: startLocation!.coordinate.latitude, lon: startLocation!.coordinate.longitude)
        }
    }
    
    func centerMapIn(lat:Double,lon:Double){
        let latitude:CLLocationDegrees = lat
        let longitude:CLLocationDegrees = lon
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        let latDelta:CLLocationDegrees = 0.01
        let lonDelta:CLLocationDegrees = 0.01
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        if mapView.region.span.latitudeDelta <= 0.01 {
            var region = self.mapView.region
            region.center = location
        } else {
            var region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
            region.center = location
            self.mapView.setRegion(region, animated: true)
        }
    }
}



extension PinsViewController: TCPickerViewDelegate {
    func pickerView(_ pickerView: TCPickerView, didSelectRowAtIndex index: Int) {

    }
}

extension PinsViewController: FilterViewControllerDelegate {
    func filtersChanged(filters: [String]) {
        selectedTypes = filters
        self.removePins()
        self.addPinsBySelectedDate()
    }
}
