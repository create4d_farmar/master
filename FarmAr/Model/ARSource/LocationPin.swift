//
//  LocationPin.swift
//  FarmAr
//
//  Created by Vlad on 13.12.2017.
//  Copyright © 2017 Vlad. All rights reserved.
//

import Foundation
import MapKit
import UIKit

class LocationPin: MKPointAnnotation {
    var color: UIColor?
    var userPin: [String: Any]?
    var geoPin: [String: Any]?
    var analyticsString: String?
}
