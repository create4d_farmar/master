//
//  LocationNode.swift

import Foundation
import SceneKit
import CoreLocation

///A location node can be added to a scene using a coordinate.
///Its scale and position should not be adjusted, as these are used for scene layout purposes
///To adjust the scale and position of items within a node, you can add them to a child node and adjust them there
open class LocationNode: SCNNode {
    ///Location can be changed and confirmed later by SceneLocationView.
    public var location: CLLocation!
    
    ///Whether the location of the node has been confirmed.
    ///This is automatically set to true when you create a node using a location.
    ///Otherwise, this is false, and becomes true once the user moves 100m away from the node,
    ///except when the locationEstimateMethod is set to use Core Location data only,
    ///as then it becomes true immediately.
    public var locationConfirmed = false
    
    ///Whether a node's position should be adjusted on an ongoing basis
    ///based on its' given location.
    ///This only occurs when a node's location is within 100m of the user.
    ///Adjustment doesn't apply to nodes without a confirmed location.
    ///When this is set to false, the result is a smoother appearance.
    ///When this is set to true, this means a node may appear to jump around
    ///as the user's location estimates update,
    ///but the position is generally more accurate.
    ///Defaults to true.
    public var continuallyAdjustNodePositionWhenWithinRange = true
    
    ///Whether a node's position and scale should be updated automatically on a continual basis.
    ///This should only be set to false if you plan to manually update position and scale
    ///at regular intervals. You can do this with `SceneLocationView`'s `updatePositionOfLocationNode`.
    public var continuallyUpdatePositionAndScale = true
    
    public init(location: CLLocation?) {
        self.location = location
        self.locationConfirmed = location != nil
        super.init()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

open class LocationAnnotationNode: LocationNode {
    ///An image to use for the annotation
    ///When viewed from a distance, the annotation will be seen at the size provided
    ///e.g. if the size is 100x100px, the annotation will take up approx 100x100 points on screen.
    public let image: UIImage
    
    ///Subnodes and adjustments should be applied to this subnode
    ///Required to allow scaling at the same time as having a 2D 'billboard' appearance
    public let annotationNode: SCNNode
    
    ///Whether the node should be scaled relative to its distance from the camera
    ///Default value (false) scales it to visually appear at the same size no matter the distance
    ///Setting to true causes annotation nodes to scale like a regular node
    ///Scaling relative to distance may be useful with local navigation-based uses
    ///For landmarks in the distance, the default is correct
    public var scaleRelativeToDistance = false
    
    
    //MARK: Pin
    public init(location: CLLocation?, color: UIColor, image: UIImage) {
        self.image = image.maskWithColor(color: color)

        let plane = SCNPlane(width: image.size.width / 300, height: image.size.height / 300)
        plane.firstMaterial!.diffuse.contents = self.image
        plane.firstMaterial!.lightingModel = .constant

        annotationNode = SCNNode()
        annotationNode.geometry = plane

        super.init(location: location)

        let billboardConstraint = SCNBillboardConstraint()
        billboardConstraint.freeAxes = SCNBillboardAxis.Y
        constraints = [billboardConstraint]

        addChildNode(annotationNode)
    }
    
    //MARK: Image Pin
    public init(location: CLLocation?, image: UIImage?) {
    
        if let realImage = image {
            self.image = realImage
        } else {
            self.image = UIImage.init(color: UIColor.gray, size: CGSize(width: 200, height: 100))!
        }
        
        let plane = SCNPlane(width: 2, height: 1)
        plane.firstMaterial!.diffuse.contents = self.image
        plane.firstMaterial!.lightingModel = .constant
        
        annotationNode = SCNNode()
        annotationNode.geometry = plane
    
        super.init(location: location)
    
        let billboardConstraint = SCNBillboardConstraint()
        billboardConstraint.freeAxes = SCNBillboardAxis.Y
        constraints = [billboardConstraint]
        
        addChildNode(annotationNode)
    }
    
    public init(location: CLLocation?, pinImage: UIImage!) {
        
        image = pinImage
        annotationNode = SCNNode()
        
        let pinNode = SCNNode()
        let logo = SCNBox(width: 1.5,
                          height: 1.5,
                          length: 0.01,
                          chamferRadius: 0.1)
        
        pinNode.geometry = logo
        super.init(location: location)
        
        let shape = SCNPhysicsShape(geometry: logo, options: nil)
        pinNode.physicsBody = SCNPhysicsBody(type: .static, shape: shape)
        pinNode.physicsBody?.isAffectedByGravity = false
        
        let images = [image, // front
            UIColor.clear, // right
            UIColor.clear, // back
            UIColor.clear, // left
            UIColor.clear, // top
            UIColor.clear] as [Any] // bottom
        
        let sideMaterials = images.map { color -> SCNMaterial in
            let material = SCNMaterial()
            material.diffuse.contents = color
            material.locksAmbientWithDiffuse = true
            return material
        }

        pinNode.geometry?.materials  = sideMaterials //Array(repeating: material, count: 6)
        let bc = SCNBillboardConstraint()
        bc.freeAxes = .Y
        pinNode.constraints = [bc]
        
        
        annotationNode.addChildNode(pinNode)
        
        addChildNode(annotationNode)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
