//
//  DataManager.swift
//  farmAR
//
//  Created by Vlad on 09/08/2018.
//  Copyright © 2018 Vlad. All rights reserved.
//

import Foundation
let kUserPins = "Saved_UserPins"
let kGeoData = "Saved_GeoData"

class DataManager: NSObject {
    public static let shared = DataManager()
    
    private func cleanDictFromNulls(dict: [String: Any]) -> [String: Any] {
        var cleanDict = dict
        for key in dict.keys {
            if let newDict = dict[key] as? [String: Any] {
                cleanDict[key] = cleanDictFromNulls(dict: newDict)
            } else if let _ = dict[key] as? NSNull {
                cleanDict.removeValue(forKey: key)
            }
        }
        return cleanDict
    }
    
    func saveGeoData(_ data: [String: Any]) {
        let clearData = cleanDictFromNulls(dict: data)
        UserDefaults.standard.set(clearData, forKey: kGeoData)
    }
    
    func getGeoData() -> [String: Any] {
        if let geoData = UserDefaults.standard.object(forKey: kGeoData) as? [String: Any] {
            return geoData
        }
        return [String: Any]()
    }
    
    func saveUserPins(_ pins: [[String: Any]]) {
        let clearPins = pins.map{cleanDictFromNulls(dict: $0)}
        UserDefaults.standard.set(clearPins, forKey: kUserPins)
    }
    
    func getUserPins() -> [[String: Any]] {
        if let pins = UserDefaults.standard.object(forKey: kUserPins) as? [[String: Any]] {
            return pins
        }
        return [[String: Any]]()
    }
}
