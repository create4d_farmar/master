//
//  ApiManager.swift
//  farmAR
//
//  Created by Vlad on 01.05.2018.
//  Copyright © 2018 Vlad. All rights reserved.
//

import Foundation
import Alamofire

class ApiManager: NSObject {
    public static let shared = ApiManager()
    
    //MARK: private methods
    private func headers() -> HTTPHeaders {
        var headers: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "application/json"
        ]
        if UserManager.shared.isLogening() {
            if let client = UserManager.shared.user?["client"] as? String {
                headers["client"] = client
            }
            if let uid = UserManager.shared.user?["uid"] as? String {
                headers["uid"] = uid
            }
            if let token = UserManager.shared.user?["access-token"] as? String {
                headers["access-token"] = token
            }
        }
        return headers
    }
    
    private func get(url: URL, parameters:[String: Any]?, successHandler: @escaping(DataResponse<Any>)->(), fail: @escaping(Error) -> (), apiFail: @escaping(String) -> ()) -> Void {

        Alamofire.request(url, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: self.headers())
            .responseJSON { response in
                if let error = response.error {
                    fail(error)
                }
                if let result = response.result.value as? [String: Any] {
                    if let errors = result["errors"] as? [String] {
                        if let errorStr = errors.first {
                            apiFail(errorStr)
                        }
                    }
                }
                successHandler(response)
        }
    }
    
    private func post(url: URL, parameters:[String: Any]?, successHandler: @escaping(DataResponse<Any>)->(), fail: @escaping(Error) -> (), apiFail: @escaping(String) -> ()) -> Void {
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: self.headers())
            .responseJSON { response in
                if let error = response.error {
                    fail(error)
                }
                if let result = response.result.value as? [String: Any] {
                    if let errors = result["errors"] as? [String] {
                        if let errorStr = errors.first {
                            apiFail(errorStr)
                        }
                    }
                }
                successHandler(response)
        }
    }
    
    private func patch(url: URL, parameters:[String: Any]?, successHandler: @escaping(DataResponse<Any>)->(), fail: @escaping(Error) -> (), apiFail: @escaping(String) -> ()) -> Void {
        
        Alamofire.request(url, method: .patch, parameters: parameters, encoding: JSONEncoding.default, headers: self.headers())
            .responseJSON { response in
                if let error = response.error {
                    fail(error)
                }
                if let result = response.result.value as? [String: Any] {
                    if let errors = result["errors"] as? [String] {
                        if let errorStr = errors.first {
                            apiFail(errorStr)
                        }
                    }
                }
                successHandler(response)
        }
    }
    
    private func delete(url: URL, parameters:[String: Any]?, successHandler: @escaping(DataResponse<Any>)->(), fail: @escaping(Error) -> (), apiFail: @escaping(String) -> ()) -> Void {
        
        Alamofire.request(url, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: self.headers())
            .responseJSON { response in
                if let error = response.error {
                    fail(error)
                }
                if let result = response.result.value as? [String: Any] {
                    if let errors = result["errors"] as? [String] {
                        if let errorStr = errors.first {
                            apiFail(errorStr)
                        }
                    }
                }
                successHandler(response)
        }
    }
    
    //MARK: Api Methods
    //MARK: Auth/Login
    func signUp(email:String, password:String, success: @escaping([String: Any])->(), fail: @escaping(Error) -> (), customError: @escaping(String) -> ()) -> Void {
        let requestUrl = URL(string:"\(mainURL_Api)/auth")!
        let parameters = ["email": email,
                          "password": password]
        
        self.post(url: requestUrl, parameters: parameters, successHandler: { response in
            if let status = response.response?.statusCode {
                switch(status) {
                case 200:
                    print("signUp success")
                    if let result = response.result.value {
                        if let JSON = result as? [String: Any] {
                            if let data = JSON["data"] as? [String: Any] {
                                if let user = data["user"] as? [String: Any] {
                                    var tempData = user
                                    if let token = response.response?.allHeaderFields["access-token"] as? String {
                                        tempData["access-token"] = token
                                    }
                                    if let client = response.response?.allHeaderFields["client"] as? String {
                                        tempData["client"] = client
                                    }
                                    success(tempData)
                                }
                            }
                        }
                    }
                    break
                default:
                    customError("signUp error with response status: \(status)")
                    break
                }
                
            }
        }, fail: { (error) in
            fail(error)
        }, apiFail: { (errorStr) in
            customError(errorStr)
        })
        
    }
    
    func signIn(email:String, password:String, success: @escaping([String: Any])->(), fail: @escaping(Error) -> (), customError: @escaping(String) -> ()) -> Void {
        let requestUrl = URL(string:"\(mainURL_Api)/auth/sign_in")!
        let parameters = ["email": email,
                          "password": password]
        
        self.post(url: requestUrl, parameters: parameters, successHandler: { response in
            if let status = response.response?.statusCode {
                switch(status){
                case 200:
                    print("signIn success")
                    if let result = response.result.value {
                        if let JSON = result as? [String: Any] {
                            if let data = JSON["data"] as? [String: Any] {
                                if let user = data["user"] as? [String: Any] {
                                    var tempData = user
                                    if let token = response.response?.allHeaderFields["access-token"] as? String {
                                        tempData["access-token"] = token
                                    }
                                    if let client = response.response?.allHeaderFields["client"] as? String {
                                        tempData["client"] = client
                                    }
                                    success(tempData)
                                }
                            }
                        }
                    } else {
                        customError("signIn fail: response.result.value as? [String: Any]")
                    }
                    break
                default:
                    customError("signIn error with response status: \(status)")
                    break
                }
                
            }
        }, fail: { (error) in
            fail(error)
        }, apiFail: { (errorStr) in
            customError(errorStr)
        })
    }
    
    //MARK: User
    func getUser(success: @escaping([String: Any])->(), fail: @escaping(Error) -> (), customError: @escaping(String) -> ()) -> Void {
        let requestUrl = URL(string:"\(mainURL_Api)/user")!
        
        self.get(url: requestUrl, parameters: nil, successHandler: { (response) in
            if let result = response.result.value as? [String: Any] {
                if let user = result["user"] as? [String: Any] {
                    var tempData = user
                    if let token = response.response?.allHeaderFields["access-token"] as? String {
                        tempData["access-token"] = token
                    }
                    if let client = response.response?.allHeaderFields["client"] as? String {
                        tempData["client"] = client
                    }
                    success(tempData)
                }
            }
        }, fail: { (error) in
            fail(error)
        }, apiFail: { (errorStr) in
            customError(errorStr)
        })
    }
    
    func editUser(userName: String, userImage: UIImage?, success: @escaping([String: Any])->(), fail: @escaping(Error) -> (), customError: @escaping(String) -> ()) -> Void {
        let requestUrl = URL(string:"\(mainURL_Api)/user")!
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if let img = userImage {
                    let imgData = UIImageJPEGRepresentation(img,1)!
                    multipartFormData.append(imgData, withName: "avatar", fileName: "avatar.jpg", mimeType: "image/jpeg")
                }
                multipartFormData.append((userName as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: "name")
        }, to: requestUrl, method: .patch, headers: self.headers()) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if let status = response.response?.statusCode {
                        switch(status){
                        case 200:
                            if let result = response.result.value as? [String: Any] {
                                if let user = result["user"] as? [String: Any] {
                                    var tempData = user
                                    if let token = response.response?.allHeaderFields["access-token"] as? String {
                                        tempData["access-token"] = token
                                    }
                                    if let client = response.response?.allHeaderFields["client"] as? String {
                                        tempData["client"] = client
                                    }
                                    success(tempData)
                                }
                            } else {
                                customError("editUser fail: response.result.value as? [String: Any]")
                            }
                            break
                        default:
                            customError("editUser error with response status:\(status)")
                            break
                        }
                    }
                }
                break
            case .failure(let encodingError):
                fail(encodingError)
                break
            }
        }
    }
    
    //MARK: Farm
    func createFarm(latitude: Double, longitude: Double, name: String, success: @escaping([String: Any])->(), fail: @escaping(Error) -> (), customError: @escaping(String) -> ()) -> Void {
        let requestUrl = URL(string:"\(mainURL_Api)/farm")!
        let params = ["latitude": latitude,
                      "longitude": longitude,
                      "name": name] as [String : Any]
        self.post(url: requestUrl, parameters: params, successHandler: { (response) in
            if let result = response.result.value as? [String: Any] {
                if let farm = result["farm"] as? [String: Any] {
                    success(farm)
                }
            }
        }, fail: { (error) in
            fail(error)
        }, apiFail: { (errorStr) in
            customError(errorStr)
        })
    }
    
    func createFarm(polygon: [[String: Any]], name: String, latitude: Double, longitude: Double, success: @escaping([String: Any])->(), fail: @escaping(Error) -> (), customError: @escaping(String) -> ()) -> Void {
        let requestUrl = URL(string:"\(mainURL_Api)/farm")!
        let params = ["polygon" : polygon,
                      "name": name,
                      "latitude": latitude,
                      "longitude": longitude] as [String : Any]
        self.post(url: requestUrl, parameters: params, successHandler: { (response) in
            if let result = response.result.value as? [String: Any] {
                if let farm = result["farm"] as? [String: Any] {
                    success(farm)
                }
            }
        }, fail: { (error) in
            fail(error)
        }, apiFail: { (errorStr) in
            customError(errorStr)
        })
    }
    
    func getFarm(success: @escaping([String: Any])->(), fail: @escaping(Error) -> (), customError: @escaping(String) -> ()) -> Void {
        let requestUrl = URL(string:"\(mainURL_Api)/farm")!
        
        self.get(url: requestUrl, parameters: nil, successHandler: { (response) in
            if let result = response.result.value as? [String: Any] {
                if let farm = result["farm"] as? [String: Any] {
                    success(farm)
                }
            }
        }, fail: { (error) in
            fail(error)
        }, apiFail: { (errorStr) in
            customError(errorStr)
        })
    }
    
    func editFarm(polygon: [[String: Any]], latitude: Double, longitude: Double, name: String, success: @escaping([String: Any])->(), fail: @escaping(Error) -> (), customError: @escaping(String) -> ()) -> Void {
        let requestUrl = URL(string:"\(mainURL_Api)/farm")!
        let params = ["polygon" : polygon,
                      "latitude": latitude,
                      "longitude": longitude,
                      "name": name] as [String : Any]
        self.patch(url: requestUrl, parameters: params, successHandler: { (response) in
            if let result = response.result.value as? [String: Any] {
                if let farm = result["farm"] as? [String: Any] {
                    success(farm)
                }
            }
        }, fail: { (error) in
            fail(error)
        }, apiFail: { (errorStr) in
            customError(errorStr)
        })
    }
    
    //MARK: UserPins
    func getUserPins(success: @escaping([[String: Any]])->(), fail: @escaping(Error) -> (), customError: @escaping(String) -> ()) -> Void {
        let requestUrl = URL(string:"\(mainURL_Api)/user_pins")!
        
        self.get(url: requestUrl, parameters: nil, successHandler: { (response) in
            if let result = response.result.value as? [String: Any]  {
                if let pins = result["user_pins"] as? [[String: Any]] {
                    success(pins)
                }
            }
        }, fail: { (error) in
            fail(error)
        }, apiFail: { (errorStr) in
            customError(errorStr)
        })
    }
    
    func getUserPinById(id: Int, success: @escaping([String: Any])->(), fail: @escaping(Error) -> (), customError: @escaping(String) -> ()) -> Void {
        let requestUrl = URL(string:"\(mainURL_Api)/user_pins/\(id)")!
        
        self.get(url: requestUrl, parameters: nil, successHandler: { (response) in
            if let result = response.result.value {
                if let pin = result as? [String: Any] {
                    success(pin)
                }
            }
        }, fail: { (error) in
            fail(error)
        }, apiFail: { (errorStr) in
            customError(errorStr)
        })
    }
    
    func createUserPin(pin: [String: Any], pinImage: UIImage?, success: @escaping([String: Any])->(), fail: @escaping(Error) -> (), customError: @escaping(String) -> ()) -> Void {
        let requestUrl = URL(string:"\(mainURL_Api)/user_pins")!
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if let img = pinImage {
                    let imgData = UIImageJPEGRepresentation(img,1)!
                    multipartFormData.append(imgData, withName: "image", fileName: "pinImage.jpg", mimeType: "image/jpeg")
                }
                
                for (key, value) in pin {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
        }, to: requestUrl, method: .post, headers: self.headers()) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if let status = response.response?.statusCode {
                        switch(status){
                        case 201:
                            print("createUserPin success")
                            if let result = response.result.value as? [String: Any] {
                                if let newPin = result["user_pin"] as? [String: Any]  {
                                    success(newPin)
                                } else {
                                    customError("createUserPin fail: response.result.value as? [String: Any]")
                                }
                            }
                            
                            break
                        default:
                            customError(String(status))
                            break
                        }
                    }
                }
                break
            case .failure(let encodingError):
                fail(encodingError)
                break
            }
        }
    }
    
    func editUserPinById(id: Int, pin: [String: Any], pinImage: UIImage?, success: @escaping([String: Any])->(), fail: @escaping(Error) -> (), customError: @escaping(String) -> ()) -> Void {
        let requestUrl = URL(string:"\(mainURL_Api)/user_pins/\(id)")!
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if let img = pinImage {
                    let imgData = UIImageJPEGRepresentation(img,1)!
                    multipartFormData.append(imgData, withName: "image", fileName: "pinImage.jpg", mimeType: "image/jpeg")
                }
                
                for key in pin.keys {
                    if let value = pin["key"] as? String {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    } else if let value = pin["key"] as? Double {
                        multipartFormData.append((NSNumber(value: value) as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                }
                
        }, to: requestUrl, method: .patch, headers: self.headers()) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if let status = response.response?.statusCode {
                        switch(status){
                        case 200:
                            print("editUserPinById success")
                            if let result = response.result.value as? [String: Any] {
                                if let newPin = result["user_pin"] as? [String: Any]  {
                                    success(newPin)
                                } else {
                                    customError("createUserPin fail: response.result.value as? [String: Any]")
                                }
                            }
                            break
                        default:
                            customError(String(status))
                            break
                        }
                    }
                }
                break
            case .failure(let encodingError):
                fail(encodingError)
                break
            }
        }
    }
    
    func deleteUserPinById(id: Int, success: @escaping()->(), fail: @escaping(Error) -> (), customError: @escaping(String) -> ()) -> Void {
        let requestUrl = URL(string:"\(mainURL_Api)/user_pins/\(id)")!
        self.delete(url: requestUrl, parameters: nil, successHandler: { (response) in
            if let status = response.response?.statusCode {
                switch(status){
                case 200:
                    print("deleteUserPinById success")
                    success()
                    break
                default:
                    customError(String(status))
                }
            }
        }, fail: { (error) in
            fail(error)
        }, apiFail: { (errorStr) in
            customError(errorStr)
        })
    }
    
    //MARK: GeoPins
    func getGeoPins(success: @escaping([String: Any])->(), fail: @escaping(Error) -> (), customError: @escaping(String) -> ()) -> Void {
        let requestUrl = URL(string:"\(mainURL_Api)/farm/geo_pins")!
        
        self.get(url: requestUrl, parameters: nil, successHandler: { (response) in
            if let result = response.result.value as? [String: Any]  {
                if let geoData = result["geo_pins"] as? [String: Any] {
                     success(geoData)
                }
            }
        }, fail: { (error) in
            fail(error)
        }, apiFail: { (errorStr) in
            customError(errorStr)
        })
    }
}
