//
//  UserManager.swift
//  farmAR
//
//  Created by Vlad on 01.05.2018.
//  Copyright © 2018 Vlad. All rights reserved.
//

import Foundation

    let kUserKey = "Saved_User"
    let kUserUpdateNotif = "user_updated"

class UserManager: NSObject {
    public static let shared = UserManager()
    
    var user: [String: Any]?
    
    override init() {
        super.init()
        if let user = UserDefaults.standard.object(forKey: kUserKey) as? [String: Any] {
            self.user = user
        }
    }
    
    private func cleanDictFromNulls(dict: [String: Any]) -> [String: Any] {
        var cleanDict = dict
        for key in dict.keys {
            if let newDict = dict[key] as? [String: Any] {
                cleanDict[key] = cleanDictFromNulls(dict: newDict)
            } else if let _ = dict[key] as? NSNull {
                cleanDict.removeValue(forKey: key)
            }
        }
        return cleanDict
    }
    
    func isLogening() -> Bool {
        return user != nil
    }
    
    func isPremium() -> Bool {
        if let prem = user!["premium"] as? Bool {
            return prem
        } else {
            return false
        }
    }
    
    func isHaveFarm() -> Bool {
        if user?["farm_id"] != nil || userFarm() != nil {
            return true
        }
        return false
    }
    
    func userFarm() -> [String: Any]? {
        if let farm = user?["farm"] as? [String: Any] {
            return farm
        }
        return nil
    }
    
    func setUserFarm(farm: [String: Any]) {
        let cleanFarm = cleanDictFromNulls(dict: farm)
        user!["farm"] = cleanFarm
        UserDefaults.standard.set(self.user, forKey: kUserKey)
        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: kUserUpdateNotif), object: nil)
    }
    
    func setUser(user: [String: Any]) {
        let cleanUser = cleanDictFromNulls(dict: user)
       
        if let farm = userFarm() {
            self.user = cleanUser
            setUserFarm(farm: farm)
        } else {
            self.user = cleanUser
        }
        
        UserDefaults.standard.set(self.user, forKey: kUserKey)
        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: kUserUpdateNotif), object: nil)
    }
    
    
    
    func signOutUser() {
        user = nil
        UserDefaults.standard.removeObject(forKey: kUserKey)
    }
    
    
}
