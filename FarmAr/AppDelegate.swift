//
//  AppDelegate.swift
//  FarmAr
//
//  Created by Vlad on 09.12.2017.
//  Copyright © 2017 Vlad. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FBSDKCoreKit
import FBSDKLoginKit
import Fabric
import Crashlytics
import GoogleMaps
import GooglePlaces
import Reachability


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var photoPicking: Bool = false
    
    let reachability = Reachability()!
    
    class func getDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func logOut() {
        try! Auth.auth().signOut()
        if let _ = FBSDKAccessToken.current() {
            FBSDKLoginManager().logOut()
        }
        UserManager.shared.signOutUser()
        setState()
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Fabric.with([Crashlytics.self])
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true
        
        GMSServices.provideAPIKey("AIzaSyCBxiU6pV4cqr0nvv_RLbfMx29RHlfHwQY")
        GMSPlacesClient.provideAPIKey("AIzaSyCBxiU6pV4cqr0nvv_RLbfMx29RHlfHwQY")
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
        setState()
        return true
    }
    
    func setState() {
        let storyboard = UIStoryboard(name: "Main",  bundle: Bundle.main)
        if UserManager.shared.isLogening() {
            let mainController = MainViewController()
            self.window?.rootViewController = mainController
        } else {
            let loginController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.window?.rootViewController = loginController
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String,
            annotation:options[UIApplicationOpenURLOptionsKey.annotation])
        return handled
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        reachability.stopNotifier()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
   
    //MARK: Support Orientation for Window
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if photoPicking {
            return .all
        }
        return .landscape
    }


}

