//
//  FarmArTextField.swift
//  FarmAr
//
//  Created by Vlad on 09.12.2017.
//  Copyright © 2017 Vlad. All rights reserved.
//

import Foundation
import UIKit

class FarmArTextField: UITextField {
    
    var errorLabel : UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.returnKeyType = .done
        self.font = UIFont.systemFont(ofSize: 14)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if errorLabel == nil {
            let y = CGFloat(2/3*self.frame.height)
            let height = CGFloat(1/3*self.frame.height)
            errorLabel = UILabel.init(frame: CGRect(x:0, y:y, width:self.frame.width - 10, height:height))
            errorLabel.textAlignment = .right
            errorLabel.font = UIFont.systemFont(ofSize: 8)
            errorLabel.textColor = UIColor.red
            self.addSubview(errorLabel)
        }
    }
    
    func setErrorMessage(_ errorMessage:String) {
        self.errorLabel.text = errorMessage
        self.layer.borderColor = UIColor.red.cgColor
        self.layer.borderWidth = 1.0
    }
    
    func validationSuccessful() {
        self.errorLabel.text = ""
        self.layer.borderWidth = 0.0
    }
}
